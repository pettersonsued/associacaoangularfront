import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Alergia } from 'src/app/Model/Alergia';
import { Aluno } from 'src/app/Model/Aluno';
import { Cartorio } from 'src/app/Model/Cartorio';
import { Cidade } from 'src/app/Model/Cidade';
import { Endereco } from 'src/app/Model/Endereco';
import { Escola } from 'src/app/Model/Escola';
import { Irmao } from 'src/app/Model/Irmao';
import { Mae } from 'src/app/Model/Mae';
import { Pai } from 'src/app/Model/Pai';
import { PessoaFisica } from 'src/app/Model/PessoaFisica';
import { ProblemaSaude } from 'src/app/Model/ProblemaSaude';
import { Registro } from 'src/app/Model/Registro';
import { Rua } from 'src/app/Model/Rua';
import { AlergiaService } from 'src/app/Service/alergia.service';
import { AlunoService } from 'src/app/Service/aluno.service';
import { CartorioService } from 'src/app/Service/cartorio.service';
import { CidadeService } from 'src/app/Service/cidade.service';
import { EscolaService } from 'src/app/Service/escola.service';
import { IrmaoService } from 'src/app/Service/irmao.service';
import { MaeService } from 'src/app/Service/mae.service';
import { PaiService } from 'src/app/Service/pai.service';
import { ProblesaudeService } from 'src/app/Service/problesaude.service';
import { RegistroService } from 'src/app/Service/registro.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-atualizar-aluno',
  templateUrl: './atualizar-aluno.component.html',
  styleUrls: ['./atualizar-aluno.component.css']
})

              ////////CLASSE COMPONENTE USADO PARA ATUALIZAR UMA ALUNO////////

export class AtualizarAlunoComponent implements OnInit {

  aluno:Aluno=new Aluno();
  endereco:Endereco = new Endereco();
  pessoa:PessoaFisica=new PessoaFisica(); 
  registro:Registro=new Registro();
  ruas:Rua[]=[];
  rua:Rua=new Rua();
  mae:Mae=new Mae();
  maes:Mae[]=[];
  pai:Pai=new Pai();
  pais:Pai[]=[];
  cidade:Cidade=new Cidade();
  cidades: Cidade[]=[];
  escola:Escola=new Escola();
  escolas:Escola[]=[];
  cartorio:Cartorio=new Cartorio();
  cartorios:Cartorio[]=[];
  irmaos:Irmao[]=[];
  meusirmaos:Irmao[]=[];
  problemas:ProblemaSaude[]=[];
  meusproblesaudes:ProblemaSaude[]=[];
  alergias:Alergia[]=[];
  minhasalergias:Alergia[]=[];
  orgaoExpedidorRgs = ['POLÍCIA CIVIL', 'OUTRA'];
  bolsas = ['SIM', 'NÃO'];
  sexos = ['MASCULINO', 'FEMININO', 'OUTRO'];
  turnos = ['MATUTINO', 'VESPERTINO', 'NOTURNO'];
  racacores = ['BRANCO', 'PARDO', 'PRETO', 'INDIO', 'OUTRO'];
  situacoes = ['ATIVO', 'DESISTENTE', 'EM ESPERA'];
  series = ['PRIMEIRA', 'SEGUNDA', 'TERCEIRA', 'QUARTA', 'QUINTA', 'SEXTA', 'SÉTIMA', 
                   'OITAVA', 'PRIMEIRO ANO', 'SEGUDO ANO','TERCEIRO ANO', 'GRADUADO', 
                   'MESTRADO', 'DOUTORADO'];

  constructor(private alunoService:AlunoService, private ruaService:RuaService, private cidadeService:CidadeService,
              private paiService:PaiService, private maeService:MaeService, private escolaService:EscolaService,
              private registroService:RegistroService, private irmaoService:IrmaoService, private router:Router,
              private alergiaService:AlergiaService, private problesaudeService:ProblesaudeService,
              private cartorioService:CartorioService, private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

   ////////METODO USADO PARA SETAR ATRIBUTOS DO ALUNO NO FORMULÁRIO/////////

   Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.alunoService.getAlunoPorID(numberValue)
    .subscribe(data=>{  
      this.aluno=data; 

      this.endereco.id = this.aluno.pessoa.endereco.id;
      this.pessoa.id = this.aluno.pessoa.id
      this.escola.id = this.aluno.escola.id
      this.cidade.id = this.aluno.cidade.id
      this.cartorio.id = this.aluno.registro.cartorio.id
      this.registro.id = this.aluno.registro.id
      this.aluno.id = this.aluno.id;

      this.formulario = this.formBuilder.group({
        matriculaAssociacao: [this.aluno.matriculaAssociacao],
        dataMatriculaAssociacao: [this.aluno.dataMatriculaAssociacao],
        matriculaEscola: [this.aluno.matriculaEscola],
        racaCor: [this.aluno.racaCor],
        serie: [this.aluno.serie],
        turno: [this.aluno.turno],
        situacaoAssociacao: [this.aluno.situacaoAssociacao],
        bolsa: [this.aluno.bolsa],
        orgaoExpedidorRg: [this.aluno.orgaoExpedidorRg],
        dataExpedicaoRg: [this.aluno.dataExpedicaoRg],
        nome: [this.aluno.pessoa.nome],
        fone: [this.aluno.pessoa.fone],
        email: [this.aluno.pessoa.email],
        rua: [this.aluno.pessoa.endereco.rua],
        cpf: [this.aluno.pessoa.cpf],
        rg: [this.aluno.pessoa.rg],
        sexo: [this.aluno.pessoa.sexo],
        referencia: [this.aluno.pessoa.endereco.referencia],
        numero:[this.aluno.pessoa.endereco.numero],
        dataNascimento: [this.aluno.pessoa.dataNascimento],
        numeroLivro: [this.aluno.registro.numeroLivro],
        numeroFolha: [this.aluno.registro.numeroFolha],
        numeroTermo: [this.aluno.registro.numeroTermo],
        cidade: [this.aluno.cidade],
        pai: [this.aluno.pai],
        mae: [this.aluno.mae],
        escola: [this.aluno.escola],
        cartorio:[this.aluno.registro.cartorio],
        irmaos: [this.aluno.irmaos],
        problemas: [this.aluno.problemas],
        alergias:[this.aluno.alergias],
      });


        this.ruaService.getRuas().subscribe(data => {
          this.ruas = data;
          let index = this.ruas.findIndex((existeRua) => existeRua.id === this.aluno.pessoa.endereco.rua.id)
          this.ruas.splice(index, 1);
          this.ruas.unshift(this.aluno.pessoa.endereco.rua);
        })

        this.paiService.getPais().subscribe(data => {
          this.pais = data;
          let index = this.pais.findIndex((existePai) => existePai.id === this.aluno.pai.id)
          this.pais.splice(index, 1);
          this.pais.unshift(this.aluno.pai);
        })

        this.maeService.getMaes().subscribe(data => {
          this.maes = data;
          let index = this.maes.findIndex((existeMae) => existeMae.id === this.aluno.mae.id)
          this.maes.splice(index, 1);
          this.maes.unshift(this.aluno.mae);
        })

        this.cidadeService.getCidades().subscribe(data => {
          this.cidades = data;
          let index = this.cidades.findIndex((existeCidade) => existeCidade.id === this.aluno.cidade.id)
          this.cidades.splice(index, 1);
          this.cidades.unshift(this.aluno.cidade);
        })

        this.cartorioService.getCartorios().subscribe(data => {
          this.cartorios = data;
          let index = this.cartorios.findIndex((existeCartorio) => existeCartorio.id === this.aluno.registro.cartorio.id)
          this.cartorios.splice(index, 1);
          this.cartorios.unshift(this.aluno.registro.cartorio);
        })

        this.escolaService.getEscolas().subscribe(data => {
          this.escolas = data;
          let index = this.escolas.findIndex((existeEscola) => existeEscola.id === this.aluno.escola.id)
          this.escolas.splice(index, 1);
          this.escolas.unshift(this.aluno.escola);
        })
    })
  }

  ////METODO RESPONSAVEL POR ATUALIZAR A MÃE///////////

  OnSubmit(){
    this.rua = this.formulario.get('rua').value

    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cpf = this.formulario.get('cpf').value
    this.pessoa.rg = this.formulario.get('rg').value
    this.pessoa.sexo = this.formulario.get('sexo').value
    this.pessoa.dataNascimento = this.formulario.get('dataNascimento').value
    this.pessoa.endereco = this.endereco;

    this.mae = this.formulario.get('mae').value
    this.pai = this.formulario.get('pai').value
    this.cidade = this.formulario.get('cidade').value
    this.escola = this.formulario.get('escola').value
    this.cartorio = this.formulario.get('cartorio').value

    this.registro.numeroTermo = this.formulario.get('numeroTermo').value
    this.registro.numeroFolha = this.formulario.get('numeroFolha').value
    this.registro.numeroLivro = this.formulario.get('numeroLivro').value
    this.registro.cartorio = this.cartorio;
    
    this.aluno.matriculaAssociacao = this.formulario.get('matriculaAssociacao').value
    this.aluno.dataMatriculaAssociacao = this.formulario.get('dataMatriculaAssociacao').value
    this.aluno.matriculaEscola = this.formulario.get('matriculaEscola').value
    this.aluno.racaCor = this.formulario.get('racaCor').value
    this.aluno.serie = this.formulario.get('serie').value
    this.aluno.turno = this.formulario.get('turno').value
    this.aluno.situacaoAssociacao = this.formulario.get('situacaoAssociacao').value
    this.aluno.bolsa = this.formulario.get('bolsa').value
    this.aluno.orgaoExpedidorRg = this.formulario.get('orgaoExpedidorRg').value
    this.aluno.dataExpedicaoRg = this.formulario.get('dataExpedicaoRg').value

    this.aluno.escola = this.escola;
    this.aluno.cidade = this.cidade;
    this.aluno.pai = this.pai;
    this.aluno.mae = this.mae;
    this.aluno.registro = this.registro;
    this.aluno.pessoa = this.pessoa;
    this.aluno.irmaos = this.Irmaos();
    this.aluno.problemas = this.Problesaudes();
    this.aluno.alergias = this.Alergias();

    console.log(this.rua);
    
   this.alunoService.updateAluno(this.aluno)
   .subscribe(data=>{alert("ALUNO ATUALIZADO COM SUCESSO...!!!");

   this.router.navigate(["listaraluno"]);
    this.formulario.reset(this.rua);
    this.formulario.reset(this.pessoa);
    this.formulario.reset(this.endereco);
   },
   erro => {
    //alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

  Irmaos(){
    return this.irmaos;
  }

  Problesaudes(){
    return this.problemas;
  }

  Alergias(){
    return this.alergias;
  }

   CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

  BuscaRuas() {
    this.ruaService.getRuas().subscribe(data => {
      console.log(data);
      this.ruas = data;
    });
  }

  BuscaCidades() {
    this.cidadeService.getCidades().subscribe(data => {
      console.log(data);
      this.cidades = data;
    });
  }

  BuscaEscolas() {
    this.escolaService.getEscolas().subscribe(data => {
      console.log(data);
      this.escolas = data;
    });
  }

  BuscaCartorios() {
    this.cartorioService.getCartorios().subscribe(data => {
      console.log(data);
      this.cartorios = data;
    });
  }

  BuscaPais() {
    this.paiService.getPais().subscribe(data => {
      console.log(data);
      this.pais = data;
    });
  }

  BuscaMaes() {
    this.maeService.getMaes().subscribe(data => {
      console.log(data);
      this.maes = data;
    });
  }

  BuscaIrmaos() {
    this.irmaoService.getIrmaos().subscribe(data => {
      console.log(data);
      this.irmaos = data;
    });
  }

  BuscaProblesaudes() {
    this.problesaudeService.getProblesaudes().subscribe(data => {
      console.log(data);
      this.problemas = data;
    });
  }

  BuscaAlergias() {
    this.alergiaService.getAlergias().subscribe(data => {
      console.log(data);
      this.alergias = data;
    });
  }

}
