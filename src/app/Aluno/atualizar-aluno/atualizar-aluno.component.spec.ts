import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarAlunoComponent } from './atualizar-aluno.component';

describe('AtualizarAlunoComponent', () => {
  let component: AtualizarAlunoComponent;
  let fixture: ComponentFixture<AtualizarAlunoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarAlunoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarAlunoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
