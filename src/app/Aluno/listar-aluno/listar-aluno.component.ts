import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Aluno } from 'src/app/Model/Aluno';
import { AlunoService } from 'src/app/Service/aluno.service';

@Component({
  selector: 'app-listar-aluno',
  templateUrl: './listar-aluno.component.html',
  styleUrls: ['./listar-aluno.component.css']
})

           ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UMA MÃE///////// 

export class ListarAlunoComponent implements OnInit {

  alunos:Aluno[]=[];

  constructor(private alunoService:AlunoService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

   ////////BUSCA TODOS OS ALUNOS/////////

   Listar(){
    this.alunoService.getAlunos().subscribe(data=>{this.alunos=data;})
    console.log(this.alunos.length)
    this.router.navigate(["listaraluno"]);
  }

 ////////METODO DELETA OS ALUNOS/////////

  Delete(aluno:Aluno){
    this.alunoService.deleteAluno(aluno).subscribe(data=>{
    this.alunos = this.alunos.filter(p=>p!==aluno);})  
    alert("ALUNO DELETADO COM SUCESSO...!!!");
  }

  ////////METODO PASSA O ID DO ALUNO SELECIONADA NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(aluno:Aluno): void{
    localStorage.setItem("id", aluno.id.toString());
    this.router.navigate(["atualizaraluno"]); 
  }

  CadastrarAluno(){
    this.router.navigate(["adicionaraluno"]);
  }

}
