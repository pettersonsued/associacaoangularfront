import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarRuaComponent } from './listar-rua.component';

describe('ListarRuaComponent', () => {
  let component: ListarRuaComponent;
  let fixture: ComponentFixture<ListarRuaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarRuaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarRuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
