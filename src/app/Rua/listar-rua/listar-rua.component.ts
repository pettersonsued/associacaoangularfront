import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Rua } from 'src/app/Model/Rua';
import { DistritoService } from 'src/app/Service/distrito.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-listar-rua',
  templateUrl: './listar-rua.component.html',
  styleUrls: ['./listar-rua.component.css']
})

////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UMA RUA/////////

export class ListarRuaComponent implements OnInit {

  ruas:Rua[]=[];

  constructor(private ruaSevice:RuaService, private distritoService:DistritoService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

  ////////BUSCA TODOS OS  RUA /////////

  Listar(){
    this.ruaSevice.getRuas().subscribe(data=>{this.ruas=data;})
    this.router.navigate(["listarrua"]);
  }

 ////////METODO DELETA SO  RUA /////////

  Delete(rua:Rua){
    this.ruaSevice.deleteRua(rua).subscribe(data=>{
    this.ruas = this.ruas.filter(p=>p!==rua);})  
    alert("Rua Deletado Com Sucesso...!!!");
  }

  ////////METODO PASSA O ID DO RUA SELECIONADO NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(rua:Rua): void{
    localStorage.setItem("id", rua.id.toString());
    this.router.navigate(["atualizarrua"]); 
  }

  CadastrarRua(){
    //this.service.getEnderecos().subscribe(data=>{this.enderecos=data;})
    this.router.navigate(["adicionarrua"]);
  }

}
