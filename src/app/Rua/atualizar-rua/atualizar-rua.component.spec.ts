import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarRuaComponent } from './atualizar-rua.component';

describe('AtualizarRuaComponent', () => {
  let component: AtualizarRuaComponent;
  let fixture: ComponentFixture<AtualizarRuaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarRuaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarRuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
