import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Distrito } from 'src/app/Model/Distrito';
import { Rua } from 'src/app/Model/Rua';
import { DistritoService } from 'src/app/Service/distrito.service';
import { RuaService } from 'src/app/Service/rua.service';
import * as jQuery from 'jquery';

@Component({
  selector: 'app-atualizar-rua',
  templateUrl: './atualizar-rua.component.html',
  styleUrls: ['./atualizar-rua.component.css']
})

////////CLASSE COMPONENTE USADO PARA EDITAR UM RUA/////////

export class AtualizarRuaComponent implements OnInit {

  distritos:Distrito[]=[];
  distrito:Distrito=new Distrito();
  rua:Rua=new Rua(); 

  constructor(private ruaService:RuaService, private distritoService:DistritoService,
              private formBuilder:FormBuilder, private router:Router) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar(); 
  }

  ////////METODO USADO OARA SETAR O RUA NO FORMULARIO UM PEDIDO/////////

  Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.ruaService.getRuaPorID(numberValue)
    .subscribe(data=>{  
      this.rua=data;

     this.formulario = this.formBuilder.group({
        id: [this.rua.id],
        logradouro: [this.rua.logradouro],
        cep: [this.rua.cep],
        distrito: [this.rua.distrito]
        })

        this.distritoService.getDistritos().subscribe(data => {
          this.distritos = data;
          let index = this.distritos.findIndex((existeDistrito) => existeDistrito.id === this.rua.distrito.id)
          this.distritos.splice(index, 1);
          this.distritos.unshift(this.rua.distrito);
        })
    })
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO RUA///////////

  OnSubmit(){
    this.distrito = this.formulario.get('distrito').value
    this.rua.distrito = this.distrito;

    this.rua.logradouro = this.formulario.get('logradouro').value;
    this.rua.cep =this.formulario.get('cep').value; 
    
   this.ruaService.updateRua(this.rua)
   .subscribe(data=>{alert("RUA ATUALIZADA COM SUCESSO...!!!");
   this.router.navigate(["listarrua"]);
   this.formulario.reset(this.rua);
   },
   erro => {
    if(erro.status == 400) {
      alert(erro.error.titulo);
    }
  })  
  }

  CadastrarDistrito(){
    this.router.navigate(["adicionardistrito"]);
  }

}
