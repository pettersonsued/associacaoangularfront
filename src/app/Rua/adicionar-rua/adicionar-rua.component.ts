import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Distrito } from 'src/app/Model/Distrito';
import { Rua } from 'src/app/Model/Rua';
import { DistritoService } from 'src/app/Service/distrito.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-adicionar-rua',
  templateUrl: './adicionar-rua.component.html',
  styleUrls: ['./adicionar-rua.component.css']
})

////////CLASSE COMPONENTE USADO PARA ADICIONAR UMA RUA/////////

export class AdicionarRuaComponent implements OnInit {

  distritos:Distrito[]=[];
  distrito:Distrito=new Distrito();
  rua:Rua=new Rua(); 

  constructor(private ruaService:RuaService, private distritoService:DistritoService,
              private formBuilder:FormBuilder, private router:Router) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.BuscaDistritos();
    this.formulario = this.formBuilder.group({
      logradouro: [''],
      cep: [''],
      distrito: ['']
    });
  }

   ///////RETORNA OS DISTRITO PARA SELECTBOX SEREM SELECIONADO/////////////

   BuscaDistritos() {
    this.distritoService.getDistritos().subscribe(data => {
      console.log(data);
      this.distritos = data;
    });
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO RUA///////////

  OnSubmit(){
    this.distrito = this.formulario.get('distrito').value
    this.rua.distrito = this.distrito;

    this.rua.logradouro = this.formulario.get('logradouro').value;
    this.rua.cep =this.formulario.get('cep').value; 
    

    console.log(this.rua);
    console.log(this.distrito);
    
   this.ruaService.createRua(this.rua)
   .subscribe(data=>{alert("RUA CADASTRADA COM SUCESSO...!!!");

   this.router.navigate(["listarrua"]);
    this.formulario.reset(this.rua);
   },
   erro => {
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

  CadastrarDistrito(){
    this.router.navigate(["adicionardistrito"]);
  }
}


