import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarMaeComponent } from './adicionar-mae.component';

describe('AdicionarMaeComponent', () => {
  let component: AdicionarMaeComponent;
  let fixture: ComponentFixture<AdicionarMaeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdicionarMaeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarMaeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
