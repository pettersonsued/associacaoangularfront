import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Endereco } from 'src/app/Model/Endereco';
import { Mae } from 'src/app/Model/Mae';
import { PessoaFisica } from 'src/app/Model/PessoaFisica';
import { Rua } from 'src/app/Model/Rua';
import { MaeService } from 'src/app/Service/mae.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-adicionar-mae',
  templateUrl: './adicionar-mae.component.html',
  styleUrls: ['./adicionar-mae.component.css']
})

             ////////CLASSE COMPONENTE USADO PARA ADICIONAR UMA MÃE////////

export class AdicionarMaeComponent implements OnInit {

  sexos = ['MASCULINO', 'FEMININO', 'OUTRO'];
  escolaridades = ['PRIMEIRA', 'SEGUNDA', 'TERCEIRA', 'QUARTA', 'QUINTA', 'SEXTA', 'SÉTIMA', 
                   'OITAVA', 'PRIMEIRO ANO', 'SEGUDO ANO','TERCEIRO ANO', 'GRADUADO', 
                   'MESTRADO', 'DOUTORADO'];
  ruas:Rua[]=[];
  rua:Rua=new Rua();
  endereco:Endereco = new Endereco();
  pessoa:PessoaFisica=new PessoaFisica(); 
  mae:Mae=new Mae();

  constructor(private maeService:MaeService, private ruaService:RuaService, private router:Router,
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.BuscaRuas();
    this.formulario = this.formBuilder.group({
      nome: [''],
      fone: [''],
      email: [''],
      rua: [''],
      cpf: [''],
      rg: [''],
      sexo: [''],
      escolaridade: [''],
      profissao: [''],
      local_trabalho: [''],
      referencia: [''],
      numero:[''],
      dataNascimento: ['']
    });
  }

  BuscaRuas() {
    this.ruaService.getRuas().subscribe(data => {
      console.log(data);
      this.ruas = data;
    });
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO RUA///////////

  OnSubmit(){
    this.rua = this.formulario.get('rua').value

    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cpf = this.formulario.get('cpf').value
    this.pessoa.rg = this.formulario.get('rg').value
    this.pessoa.sexo = this.formulario.get('sexo').value
    this.pessoa.dataNascimento = this.formulario.get('dataNascimento').value
    this.pessoa.endereco = this.endereco;
    
    this.mae.escolaridade = this.formulario.get('escolaridade').value
    this.mae.profissao = this.formulario.get('profissao').value
    this.mae.local_trabalho = this.formulario.get('local_trabalho').value
    this.mae.pessoa = this.pessoa;

    console.log(this.rua);
    console.log(this.endereco);
    console.log(this.pessoa);
    console.log(this.mae);
    
   this.maeService.createMae(this.mae)
   .subscribe(data=>{alert("MÃE CADASTRADO COM SUCESSO...!!!");

   this.router.navigate(["listarmae"]);
    this.formulario.reset(this.mae);
   },
   erro => {
    alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

   CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

}
