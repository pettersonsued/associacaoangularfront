import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Endereco } from 'src/app/Model/Endereco';
import { Mae } from 'src/app/Model/Mae';
import { PessoaFisica } from 'src/app/Model/PessoaFisica';
import { Rua } from 'src/app/Model/Rua';
import { MaeService } from 'src/app/Service/mae.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-atualizar-mae',
  templateUrl: './atualizar-mae.component.html',
  styleUrls: ['./atualizar-mae.component.css']
})

         ////////COMPONENTE PARA ATUALIZAR MÃE/////////

export class AtualizarMaeComponent implements OnInit {

  sexos = ['MASCULINO', 'FEMININO', 'OUTRO'];
  escolaridades = ['PRIMEIRA', 'SEGUNDA', 'TERCEIRA', 'QUARTA', 'QUINTA', 'SEXTA', 'SÉTIMA', 
                   'OITAVA', 'PRIMEIRO ANO', 'SEGUDO ANO','TERCEIRO ANO', 'GRADUADO', 
                   'MESTRADO', 'DOUTORADO'];
  ruas:Rua[]=[];
  rua:Rua=new Rua();
  endereco:Endereco = new Endereco();
  pessoa:PessoaFisica=new PessoaFisica(); 
  mae:Mae = new Mae();

  constructor(private maeService:MaeService, private ruaService:RuaService, private router:Router,
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

  ////////METODO USADO PARA SETAR ATRIBUTOS DA MÃE NO FORMULÁRIO/////////

  Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.maeService.getMaePorID(numberValue)
    .subscribe(data=>{  
      this.mae=data; 

      this.endereco.id = this.mae.pessoa.endereco.id;
      this.pessoa.id = this.mae.pessoa.id
      this.mae.id = this.mae.id;

      this.formulario = this.formBuilder.group({
        nome: [this.mae.pessoa.nome],
        fone: [this.mae.pessoa.fone],
        email: [this.mae.pessoa.email],
        rua: [this.mae.pessoa.endereco.rua],
        cpf: [this.mae.pessoa.cpf],
        rg: [this.mae.pessoa.rg],
        sexo: [this.mae.pessoa.sexo],
        escolaridade: [this.mae.escolaridade],
        profissao: [this.mae.profissao],
        local_trabalho: [this.mae.local_trabalho],
        referencia: [this.mae.pessoa.endereco.referencia],
        numero:[this.mae.pessoa.endereco.numero],
        dataNascimento: [this.mae.pessoa.dataNascimento]
      });


        this.ruaService.getRuas().subscribe(data => {
          this.ruas = data;
          let index = this.ruas.findIndex((existeRua) => existeRua.id === this.mae.pessoa.endereco.rua.id)
          this.ruas.splice(index, 1);
          this.ruas.unshift(this.mae.pessoa.endereco.rua);
        })
    })
  }

  ////METODO RESPONSAVEL POR ATUALIZAR A MÃE///////////

  OnSubmit(){
    this.rua = this.formulario.get('rua').value

    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cpf = this.formulario.get('cpf').value
    this.pessoa.rg = this.formulario.get('rg').value
    this.pessoa.sexo = this.formulario.get('sexo').value
    this.pessoa.dataNascimento = this.formulario.get('dataNascimento').value
    this.pessoa.endereco = this.endereco;
    
    this.mae.escolaridade = this.formulario.get('escolaridade').value
    this.mae.profissao = this.formulario.get('profissao').value
    this.mae.local_trabalho = this.formulario.get('local_trabalho').value
    this.mae.pessoa = this.pessoa;

    console.log(this.rua);
    console.log(this.endereco);
    console.log(this.pessoa);
    console.log(this.mae);
    
   this.maeService.updateMae(this.mae)
   .subscribe(data=>{alert("MÃE ATUALIZADO COM SUCESSO...!!!");

   this.router.navigate(["listarmae"]);
    this.formulario.reset(this.rua);
    this.formulario.reset(this.pessoa);
    this.formulario.reset(this.endereco);
   },
   erro => {
    //alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

   CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

}
