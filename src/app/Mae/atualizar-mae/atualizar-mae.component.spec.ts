import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarMaeComponent } from './atualizar-mae.component';

describe('AtualizarMaeComponent', () => {
  let component: AtualizarMaeComponent;
  let fixture: ComponentFixture<AtualizarMaeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarMaeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarMaeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
