import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarMaeComponent } from './listar-mae.component';

describe('ListarMaeComponent', () => {
  let component: ListarMaeComponent;
  let fixture: ComponentFixture<ListarMaeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarMaeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarMaeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
