import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Mae } from 'src/app/Model/Mae';
import { MaeService } from 'src/app/Service/mae.service';

@Component({
  selector: 'app-listar-mae',
  templateUrl: './listar-mae.component.html',
  styleUrls: ['./listar-mae.component.css']
})

              ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UMA MÃE///////// 

export class ListarMaeComponent implements OnInit {

  maes:Mae[]=[];

  constructor(private maeService:MaeService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

  ////////BUSCA TODOS AS MÃES/////////

  Listar(){
    this.maeService.getMaes().subscribe(data=>{this.maes=data;})
    console.log(this.maes.length)
    this.router.navigate(["listarmae"]);
  }

 ////////METODO DELETA AS MÃES/////////

  Delete(mae:Mae){
    this.maeService.deleteMae(mae).subscribe(data=>{
    this.maes = this.maes.filter(p=>p!==mae);})  
    alert("MÃE DELETADO COM SUCESSO...!!!");
  }

  ////////METODO PASSA O ID DO MÃE SELECIONADA NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(mae:Mae): void{
    localStorage.setItem("id", mae.id.toString());
    this.router.navigate(["atualizarmae"]); 
  }

  CadastrarMae(){
    this.router.navigate(["adicionarmae"]);
  }

}
