import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pai } from 'src/app/Model/Pai';
import { PaiService } from 'src/app/Service/pai.service';

@Component({
  selector: 'app-listar-pai',
  templateUrl: './listar-pai.component.html',
  styleUrls: ['./listar-pai.component.css']
})

       ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UM PAI///////// 

export class ListarPaiComponent implements OnInit {

  pais:Pai[]=[];

  constructor(private paiService:PaiService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

  ////////BUSCA TODOS OS PAIS/////////

  Listar(){
    this.paiService.getPais().subscribe(data=>{this.pais=data;})
    console.log(this.pais.length)
    this.router.navigate(["listarpai"]);
  }

 ////////METODO DELETA SO PAIS /////////

  Delete(pai:Pai){
    this.paiService.deletePai(pai).subscribe(data=>{
    this.pais = this.pais.filter(p=>p!==pai);})  
    alert("PAI DELETADO COM SUCESSO...!!!");
  }

  ////////METODO PASSA O ID DO PAI SELECIONADO NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(pai:Pai): void{
    localStorage.setItem("id", pai.id.toString());
    this.router.navigate(["atualizarpai"]); 
  }

  CadastrarPai(){
    this.router.navigate(["adicionarpai"]);
  }

}
