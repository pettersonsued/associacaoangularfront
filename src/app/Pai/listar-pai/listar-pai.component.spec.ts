import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarPaiComponent } from './listar-pai.component';

describe('ListarPaiComponent', () => {
  let component: ListarPaiComponent;
  let fixture: ComponentFixture<ListarPaiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarPaiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarPaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
