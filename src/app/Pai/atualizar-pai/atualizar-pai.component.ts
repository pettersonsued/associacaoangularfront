import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Endereco } from 'src/app/Model/Endereco';
import { Pai } from 'src/app/Model/Pai';
import { PessoaFisica } from 'src/app/Model/PessoaFisica';
import { Rua } from 'src/app/Model/Rua';
import { PaiService } from 'src/app/Service/pai.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-atualizar-pai',
  templateUrl: './atualizar-pai.component.html',
  styleUrls: ['./atualizar-pai.component.css']
})

     ////////COMPONENTE E FORMULARIO PAI/////////

export class AtualizarPaiComponent implements OnInit {

  sexos = ['MASCULINO', 'FEMININO', 'OUTRO'];
  escolaridades = ['PRIMEIRA', 'SEGUNDA', 'TERCEIRA', 'QUARTA', 'QUINTA', 'SEXTA', 'SÉTIMA', 
                   'OITAVA', 'PRIMEIRO ANO', 'SEGUDO ANO','TERCEIRO ANO', 'GRADUADO', 
                   'MESTRADO', 'DOUTORADO'];
  ruas:Rua[]=[];
  rua:Rua=new Rua();
  endereco:Endereco = new Endereco();
  pessoa:PessoaFisica=new PessoaFisica(); 
  pai:Pai=new Pai();

  constructor(private paiService:PaiService, private ruaService:RuaService, private router:Router,
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

  ////////METODO USADO OARA SETAR O RUA NO FORMULARIO UM PAI/////////

  Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.paiService.getPaiPorID(numberValue)
    .subscribe(data=>{  
      this.pai=data; 

      this.endereco.id = this.pai.pessoa.endereco.id;
      this.pessoa.id = this.pai.pessoa.id
      this.pai.id = this.pai.id;

      this.formulario = this.formBuilder.group({
        nome: [this.pai.pessoa.nome],
        fone: [this.pai.pessoa.fone],
        email: [this.pai.pessoa.email],
        rua: [this.pai.pessoa.endereco.rua],
        cpf: [this.pai.pessoa.cpf],
        rg: [this.pai.pessoa.rg],
        sexo: [this.pai.pessoa.sexo],
        escolaridade: [this.pai.escolaridade],
        profissao: [this.pai.profissao],
        local_trabalho: [this.pai.local_trabalho],
        referencia: [this.pai.pessoa.endereco.referencia],
        numero:[this.pai.pessoa.endereco.numero],
        dataNascimento: [this.pai.pessoa.dataNascimento]
      });


        this.ruaService.getRuas().subscribe(data => {
          this.ruas = data;
          let index = this.ruas.findIndex((existeRua) => existeRua.id === this.pai.pessoa.endereco.rua.id)
          this.ruas.splice(index, 1);
          this.ruas.unshift(this.pai.pessoa.endereco.rua);
        })
    })
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO PAI///////////

  OnSubmit(){
    this.rua = this.formulario.get('rua').value

    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cpf = this.formulario.get('cpf').value
    this.pessoa.rg = this.formulario.get('rg').value
    this.pessoa.sexo = this.formulario.get('sexo').value
    this.pessoa.dataNascimento = this.formulario.get('dataNascimento').value
    this.pessoa.endereco = this.endereco;
    
    this.pai.escolaridade = this.formulario.get('escolaridade').value
    this.pai.profissao = this.formulario.get('profissao').value
    this.pai.local_trabalho = this.formulario.get('local_trabalho').value
    this.pai.pessoa = this.pessoa;


    console.log(this.rua);
    console.log(this.endereco);
    console.log(this.pessoa);
    console.log(this.pai);
    
   this.paiService.updatePai(this.pai)
   .subscribe(data=>{alert("PAI ATUALIZADO COM SUCESSO...!!!");

   this.router.navigate(["listarpai"]);
    this.formulario.reset(this.rua);
    this.formulario.reset(this.pessoa);
    this.formulario.reset(this.endereco);
   },
   erro => {
    //alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

   CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

}
