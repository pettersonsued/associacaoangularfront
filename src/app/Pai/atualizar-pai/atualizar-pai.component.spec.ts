import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarPaiComponent } from './atualizar-pai.component';

describe('AtualizarPaiComponent', () => {
  let component: AtualizarPaiComponent;
  let fixture: ComponentFixture<AtualizarPaiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarPaiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarPaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
