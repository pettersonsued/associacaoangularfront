import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarPaiComponent } from './adicionar-pai.component';

describe('AdicionarPaiComponent', () => {
  let component: AdicionarPaiComponent;
  let fixture: ComponentFixture<AdicionarPaiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdicionarPaiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarPaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
