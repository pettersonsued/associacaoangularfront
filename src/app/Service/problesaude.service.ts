import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ProblemaSaude } from '../Model/ProblemaSaude';

@Injectable({
  providedIn: 'root'
})

            ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA PROBLE SAÚDE//////////////// 

export class ProblesaudeService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/problemas'


  ////////////////////RETORNA LISTA DE PROBLE SAÚDE/////////////////

  getProblesaudes(){
    return this.http.get<ProblemaSaude[]>(this.url);
  }

////////////////////CRIA UM NOVO PROBLE SAÚDE/////////////////

   createProblesaude(problesaude:ProblemaSaude) {
     return this.http.post<ProblemaSaude>(this.url,problesaude, {observe: 'response'});
   }

   ////////////////////REMOVE PROBLE SAÚDE////////////////

   deleteProblesaude(problesaude:ProblemaSaude){
    console.log(problesaude.id); 
    return this.http.delete<ProblemaSaude>(this.url+"/"+problesaude.id);
   }

    ////////////////////ATUALIZA PROBLE SAÚDE/////////////////

  updateProblesaude(problesaude:ProblemaSaude){
    console.log(problesaude);
    console.log(problesaude.id);
     return this.http.put<ProblemaSaude>(this.url+"/"+problesaude.id, problesaude);
  }

 ////////////////////BUSCALIVRO PARA PROBLE SAÚDE/////////////////

  getProblesaudePorID(id:number){
    return this.http.get<ProblemaSaude>(this.url+"/"+id);
  }
}
