import { TestBed } from '@angular/core/testing';

import { ProblesaudeService } from './problesaude.service';

describe('ProblesaudeService', () => {
  let service: ProblesaudeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProblesaudeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
