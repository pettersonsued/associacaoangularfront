import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cartorio } from '../Model/Cartorio';

@Injectable({
  providedIn: 'root'
})

                    ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA CARTORIO////////////////  

export class CartorioService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/cartorios'; 

  ////////////////////RETORNA LISTA DE CARTÓRIOS/////////////////

  getCartorios(){
    return this.http.get<Cartorio[]>(this.url);
  }

////////////////////CRIA UM NOVO CARTÓRIO/////////////////

   createCartorio(cartorio:Cartorio) {
     return this.http.post<Cartorio>(this.url,cartorio, {observe: 'response'});
   }

   ////////////////////REMOVE CARTÓRIO/////////////////

   deleteCartorio(cartorio:Cartorio){
    console.log(cartorio.pessoa.id); 
    return this.http.delete<Cartorio>(this.url+"/"+cartorio.id);
   }

  ////////////////////ATUALIZA CARTÓRIO/////////////////

  updateCartorio(cartorio:Cartorio){
    console.log(cartorio);
    console.log(cartorio.pessoa.id);
     return this.http.put<Cartorio>(this.url+"/"+cartorio.id, cartorio);
  }

 ////////////////////BUSCA CARTÓRIO PARA ATUALIZAR/////////////////

  getCartorioPorID(id:number){
    return this.http.get<Cartorio>(this.url+"/"+id);
  }

}
