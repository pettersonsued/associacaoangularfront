import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pai } from '../Model/Pai';

@Injectable({
  providedIn: 'root'
})

     ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA PAI/////////////////

export class PaiService {

  constructor(private http:HttpClient) { }

     url='http://localhost:8080/pais'



     ////////////////////RETORNA LISTA DE PAI/////////////////

  getPais(){
    return this.http.get<Pai[]>(this.url);
  }

////////////////////CRIA UM NOVO PAI/////////////////

   createPai(pai:Pai) {
     return this.http.post<Pai>(this.url,pai, {observe: 'response'});
   }

////////////////////REMOVE PAI/////////////////

   deletePai(pai:Pai){
    console.log(pai.pessoa.id); 
    return this.http.delete<Pai>(this.url+"/"+pai.id);
   }

  ////////////////////ATUALIZA PAI/////////////////

  updatePai(pai:Pai){
    console.log(pai);
    console.log(pai.pessoa.id);
     return this.http.put<Pai>(this.url+"/"+pai.id, pai);
  }

 ////////////////////BUSCA PAI PARA ATUALIZAR/////////////////

  getPaiPorID(id:number){
    return this.http.get<Pai>(this.url+"/"+id);
  }
}
