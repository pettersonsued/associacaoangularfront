import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Endereco } from '../Model/Endereco';

@Injectable({
  providedIn: 'root'
})

///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA ENDEREÇO/////////////////

export class EnderecoService {

  constructor(private http:HttpClient) {}

  url='http://localhost:8080/enderecos'; 

  ////////////////////RETORNA LISTA DE ENDEREÇOs/////////////////

  getEnderecos(){
    return this.http.get<Endereco[]>(this.url);
  }

////////////////////CRIA UM NOVO ENDEREÇO/////////////////

   createEndereco(endereco:Endereco){
     return this.http.post<Endereco>(this.url,endereco);
   }

////////////////////REMOVE ENDEREÇO/////////////////

   deleteEndereco(endereco:Endereco){
    console.log(endereco.id); 
    return this.http.delete<Endereco>(this.url+"/"+endereco.id);
   }

  ////////////////////ATUALIZA ENDEREÇO/////////////////

  updateEndereco(endereco:Endereco){
    console.log(endereco);
    console.log(endereco.id);
     return this.http.put<Endereco>(this.url+"/"+endereco.id, endereco);
  }

 ////////////////////BUSCA ENDEREÇO PARA ATUALIZAR/////////////////

  getEnderecoPorID(id:number){
    return this.http.get<Endereco>(this.url+"/"+id);
  }
  
}
