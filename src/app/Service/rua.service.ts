import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Rua } from '../Model/Rua';

@Injectable({
  providedIn: 'root'
})

///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA RUA/////////////////

export class RuaService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/ruas'; 

   ////////////////////RETORNA LISTA DE RUA/////////////////

   getRuas(){
    return this.http.get<Rua[]>(this.url);
  }

////////////////////CRIA UM NOVO RUA/////////////////

   createRua(rua:Rua) {
     return this.http.post<Rua>(this.url,rua, {observe: 'response'});
   }

////////////////////REMOVE RUA/////////////////

   deleteRua(rua:Rua){
    console.log(rua.id); 
    return this.http.delete<Rua>(this.url+"/"+rua.id);
   }

  ////////////////////ATUALIZA RUA/////////////////

  updateRua(rua:Rua){
    console.log(rua);
    console.log(rua.id);
     return this.http.put<Rua>(this.url+"/"+rua.id, rua);
  }

 ////////////////////BUSCA RUA PARA ATUALIZAR/////////////////

  getRuaPorID(id:number){
    return this.http.get<Rua>(this.url+"/"+id);
  }
}
