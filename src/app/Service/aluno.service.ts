import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Aluno } from '../Model/Aluno';

@Injectable({
  providedIn: 'root'
})

    ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA ALUNO////////////////

export class AlunoService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/alunos'


  ////////////////////RETORNA LISTA DE ALUNO/////////////////

  getAlunos(){
    return this.http.get<Aluno[]>(this.url);
  }

////////////////////CRIA UM NOVA ALUNO/////////////////

   createAluno(aluno:Aluno) {
     return this.http.post<Aluno>(this.url,aluno, {observe: 'response'});
   }

   ////////////////////REMOVE ALUNO/////////////////

   deleteAluno(aluno:Aluno){
    console.log(aluno.id); 
    return this.http.delete<Aluno>(this.url+"/"+aluno.id);
   }

  ////////////////////ATUALIZA ALUNO/////////////////

  updateAluno(aluno:Aluno){
    console.log(aluno);
    console.log(aluno.pessoa.id);
     return this.http.put<Aluno>(this.url+"/"+aluno.id, aluno);
  }

 ////////////////////BUSCA ALUNO PARA ATUALIZAR/////////////////

  getAlunoPorID(id:number){
    return this.http.get<Aluno>(this.url+"/"+id);
  }
}
