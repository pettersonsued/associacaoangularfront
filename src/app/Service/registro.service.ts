import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Registro } from '../Model/Registro';

@Injectable({
  providedIn: 'root'
})

                    ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA REGISTRO//////////////// 

export class RegistroService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/registros'



  ////////////////////RETORNA LISTA DE REGISTRO/////////////////

  getRegistros(){
    return this.http.get<Registro[]>(this.url);
  }

////////////////////CRIA UM NOVO REGISTRO/////////////////

   createRegistro(registro:Registro) {
     return this.http.post<Registro>(this.url,registro, {observe: 'response'});
   }

   ////////////////////REMOVE REGISTRO////////////////

   deleteRegistro(registro:Registro){
    console.log(registro.id); 
    return this.http.delete<Registro>(this.url+"/"+registro.id);
   }

   ////////////////////ATUALIZA REGISTRO/////////////////

  updateRegistro(registro:Registro){
    console.log(registro);
    console.log(registro.id);
     return this.http.put<Registro>(this.url+"/"+registro.id, registro);
  }

 ////////////////////BUSCALIVRO PARA REGISTRO/////////////////

  getRegistroPorID(id:number){
    return this.http.get<Registro>(this.url+"/"+id);
  }
}
