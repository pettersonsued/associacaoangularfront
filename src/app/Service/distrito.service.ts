import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Distrito } from '../Model/Distrito';

@Injectable({
  providedIn: 'root'
})

///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA DISTRITO/////////////////

export class DistritoService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/distritos'; 


   ////////////////////RETORNA LISTA DE DISTRITO/////////////////

   getDistritos(){
    return this.http.get<Distrito[]>(this.url);
  }

////////////////////CRIA UM NOVO DISTRITO/////////////////

   createDistrito(distrito:Distrito){
     return this.http.post<Distrito>(this.url,distrito);
   }

////////////////////REMOVE DISTRITO/////////////////

   deleteDistrito(distrito:Distrito){
    console.log(distrito.id); 
    return this.http.delete<Distrito>(this.url+"/"+distrito.id);
   }

  ////////////////////ATUALIZA DISTRITO/////////////////

  updateDistrito(distrito:Distrito){
    console.log(distrito);
    console.log(distrito.id);
     return this.http.put<Distrito>(this.url+"/"+distrito.id, distrito);
  }

 ////////////////////BUSCA DISTRITO PARA ATUALIZAR/////////////////

  getDistritoPorID(id:number){
    return this.http.get<Distrito>(this.url+"/"+id);
  }
}
