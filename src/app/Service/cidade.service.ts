import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cidade } from '../Model/Cidade';

@Injectable({
  providedIn: 'root'
})

     ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA CIDADE/////////////////
   
export class CidadeService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/cidades'; 

  ////////////////////RETORNA LISTA DE CIDADE/////////////////

  getCidades(){
    return this.http.get<Cidade[]>(this.url);
  }

////////////////////CRIA UMA NOVA CIDADE/////////////////

   createCidade(cidade:Cidade){
     return this.http.post<Cidade>(this.url,cidade);
   }

////////////////////REMOVE CIDADE/////////////////

   deleteCidade(cidade:Cidade){
    console.log(cidade.id); 
    return this.http.delete<Cidade>(this.url+"/"+cidade.id);
   }

  ////////////////////ATUALIZA CIDADE/////////////////

  updateCidade(cidade:Cidade){
    console.log(cidade);
    console.log(cidade.id);
     return this.http.put<Cidade>(this.url+"/"+cidade.id, cidade);
  }

 ////////////////////BUSCA CIDADE PARA ATUALIZAR/////////////////

  getCidadePorID(id:number){
    return this.http.get<Cidade>(this.url+"/"+id);
  }
}
