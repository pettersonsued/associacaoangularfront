import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Alergia } from '../Model/Alergia';

@Injectable({
  providedIn: 'root'
})

    ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA ALERGIA////////////////

export class AlergiaService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/alergias'


   ////////////////////RETORNA LISTA DE ALERGIA/////////////////

   getAlergias(){
    return this.http.get<Alergia[]>(this.url);
  }

////////////////////CRIA UM NOVA ALERGIA/////////////////

   createAlergia(alegria:Alergia) {
     return this.http.post<Alergia>(this.url,alegria, {observe: 'response'});
   }

   ////////////////////REMOVE ALERGIA/////////////////

   deleteAlergia(alergia:Alergia){
    console.log(alergia.id); 
    return this.http.delete<Alergia>(this.url+"/"+alergia.id);
   }

  ////////////////////ATUALIZA ALERGIA/////////////////

  updateAlergia(alergia:Alergia){
    console.log(alergia);
    console.log(alergia.id);
     return this.http.put<Alergia>(this.url+"/"+alergia.id, alergia);
  }

 ////////////////////BUSCA ALERGIA PARA ATUALIZAR/////////////////

  getAlergiaPorID(id:number){
    return this.http.get<Alergia>(this.url+"/"+id);
  }
}
