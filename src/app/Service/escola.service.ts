import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Escola } from '../Model/Escola';

@Injectable({
  providedIn: 'root'
})

   ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA ESCOLA/////////////////

export class EscolaService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/escolas'; 

  

  ////////////////////RETORNA LISTA DE ESCOLA/////////////////

  getEscolas(){
    return this.http.get<Escola[]>(this.url);
  }

////////////////////CRIA UM NOVO ESCOLA/////////////////

   createEscola(escola:Escola) {
     return this.http.post<Escola>(this.url,escola, {observe: 'response'});
   }

////////////////////REMOVE ESCOLA/////////////////

   deleteEscola(escola:Escola){
    console.log(escola.pessoa.id); 
    return this.http.delete<Escola>(this.url+"/"+escola.id);
   }

  ////////////////////ATUALIZA ESCOLA/////////////////

  updateEscola(escola:Escola){
    console.log(escola);
    console.log(escola.pessoa.id);
     return this.http.put<Escola>(this.url+"/"+escola.id, escola);
  }

 ////////////////////BUSCA ESCOLA PARA ATUALIZAR/////////////////

  getEscolaPorID(id:number){
    return this.http.get<Escola>(this.url+"/"+id);
  }
}
