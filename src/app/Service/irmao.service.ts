import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Irmao } from '../Model/Irmao';

@Injectable({
  providedIn: 'root'
})

                    ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA IRMÃO/////////////////

export class IrmaoService {

  constructor(private http:HttpClient) { }

  url='http://localhost:8080/irmaos'



   ////////////////////RETORNA LISTA DE IRMÃO/////////////////

   getIrmaos(){
    return this.http.get<Irmao[]>(this.url);
  }

////////////////////CRIA UM NOVO IRMÃO/////////////////

   createIrmao(irmao:Irmao) {
     return this.http.post<Irmao>(this.url,irmao, {observe: 'response'});
   }

////////////////////REMOVE IRMÃO/////////////////

   deleteIrmao(irmao:Irmao){
    console.log(irmao.pessoa.id); 
    return this.http.delete<Irmao>(this.url+"/"+irmao.id);
   }

  ////////////////////ATUALIZA IRMÃO/////////////////

  updateIrmao(irmao:Irmao){
    console.log(irmao);
    console.log(irmao.pessoa.id);
     return this.http.put<Irmao>(this.url+"/"+irmao.id, irmao);
  }

 ////////////////////BUSCA IRMÃO PARA ATUALIZAR/////////////////

  getIrmaoPorID(id:number){
    return this.http.get<Irmao>(this.url+"/"+id);
  }
}
