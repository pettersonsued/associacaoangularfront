import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Mae } from '../Model/Mae';

@Injectable({
  providedIn: 'root'
})

                 ///////////////////CLASSE CRIA SERVIÇO DE CONEXÃO PARA MAE/////////////////

export class MaeService {

  constructor(private http:HttpClient) { }

     url='http://localhost:8080/maes'


     
     ////////////////////RETORNA LISTA DE MAE/////////////////

  getMaes(){
    return this.http.get<Mae[]>(this.url);
  }

////////////////////CRIA UM NOVO MAE/////////////////

   createMae(mae:Mae) {
     return this.http.post<Mae>(this.url,mae, {observe: 'response'});
   }

////////////////////REMOVE MAE/////////////////

   deleteMae(mae:Mae){
    console.log(mae.pessoa.id); 
    return this.http.delete<Mae>(this.url+"/"+mae.id);
   }

  ////////////////////ATUALIZA MAE/////////////////

  updateMae(mae:Mae){
    console.log(mae);
    console.log(mae.pessoa.id);
     return this.http.put<Mae>(this.url+"/"+mae.id, mae);
  }

 ////////////////////BUSCA MAE PARA ATUALIZAR/////////////////

  getMaePorID(id:number){
    return this.http.get<Mae>(this.url+"/"+id);
  }
}
