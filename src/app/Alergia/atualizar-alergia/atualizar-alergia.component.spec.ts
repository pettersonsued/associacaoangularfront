import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarAlergiaComponent } from './atualizar-alergia.component';

describe('AtualizarAlergiaComponent', () => {
  let component: AtualizarAlergiaComponent;
  let fixture: ComponentFixture<AtualizarAlergiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarAlergiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarAlergiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
