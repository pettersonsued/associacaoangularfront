import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Alergia } from 'src/app/Model/Alergia';
import { AlergiaService } from 'src/app/Service/alergia.service';

@Component({
  selector: 'app-atualizar-alergia',
  templateUrl: './atualizar-alergia.component.html',
  styleUrls: ['./atualizar-alergia.component.css']
})

     ////////CLASSE COMPONENTE USADO PARA ATUALIZA UM ALERGIA/////////

export class AtualizarAlergiaComponent implements OnInit {

  grauperigos = ['ALTO', 'MÉDIO', 'BAIXO'];
  alergia:Alergia = new Alergia(); 

  constructor(private alergiaService:AlergiaService, private router:Router, 
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

   ////////METODO USADO OARA SETAR O ALERGIA NO FORMULARIO UM PEDIDO/////////

   Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.alergiaService.getAlergiaPorID(numberValue)
    .subscribe(data=>{  
      this.alergia=data;

     this.formulario = this.formBuilder.group({
        id: [this.alergia.id],
        nome: [this.alergia.nome],
        causa: [this.alergia.causa],
        grauPerigo: [this.alergia.grauPerigo]
        })
    })
  }

  ////METODO RESPONSAVEL POR ATUALIZAR UMA ALERGIA///////////

  OnSubmit(){
    this.alergia.nome = this.formulario.get('nome').value;
    this.alergia.causa = this.formulario.get('causa').value;
    this.alergia.grauPerigo = this.formulario.get('grauPerigo').value; 
    
   this.alergiaService.updateAlergia(this.alergia)
   .subscribe(data=>{alert("ALERGIA ATUALIZADO COM SUCESSO...!!!");
   this.router.navigate(["listaralergia"]);
   this.formulario.reset(this.alergia);
   },
   erro => {
    if(erro.status == 400) {
      alert(erro.error.titulo);
    }
  })  
  }

}
