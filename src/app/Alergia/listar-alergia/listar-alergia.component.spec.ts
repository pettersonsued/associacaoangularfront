import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarAlergiaComponent } from './listar-alergia.component';

describe('ListarAlergiaComponent', () => {
  let component: ListarAlergiaComponent;
  let fixture: ComponentFixture<ListarAlergiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarAlergiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarAlergiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
