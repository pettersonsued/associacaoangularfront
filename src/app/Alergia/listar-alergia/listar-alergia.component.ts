import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Alergia } from 'src/app/Model/Alergia';
import { AlergiaService } from 'src/app/Service/alergia.service';

@Component({
  selector: 'app-listar-alergia',
  templateUrl: './listar-alergia.component.html',
  styleUrls: ['./listar-alergia.component.css']
})

      ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UM ALERGIA/////////

export class ListarAlergiaComponent implements OnInit {

  alergias:Alergia[]=[];

  constructor(private alergiaService:AlergiaService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

   ////////BUSCA TODOS OS ALERGIA /////////

   Listar(){
    this.alergiaService.getAlergias().subscribe(data=>{this.alergias=data;})
    this.router.navigate(["listaralergia"]);
  }

 ////////METODO DELETA ALERGIA /////////

  Delete(alergia:Alergia){
    this.alergiaService.deleteAlergia(alergia).subscribe(data=>{
    this.alergias = this.alergias.filter(p=>p!==alergia);})  
    alert("ALERGIA DELETADO COM SUCESSO...!!!");
  }

  ////////METODO PASSA O ID DO ALERGIA SAÚDE SELECIONADO NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(alergia:Alergia): void{
    localStorage.setItem("id", alergia.id.toString());
    this.router.navigate(["atualizaralergia"]); 
  }

  CadastrarAlergia(){
    this.router.navigate(["adicionaralergia"]);
  }


}
