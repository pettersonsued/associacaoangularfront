import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarAlergiaComponent } from './adicionar-alergia.component';

describe('AdicionarAlergiaComponent', () => {
  let component: AdicionarAlergiaComponent;
  let fixture: ComponentFixture<AdicionarAlergiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdicionarAlergiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarAlergiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
