import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Alergia } from 'src/app/Model/Alergia';
import { AlergiaService } from 'src/app/Service/alergia.service';

@Component({
  selector: 'app-adicionar-alergia',
  templateUrl: './adicionar-alergia.component.html',
  styleUrls: ['./adicionar-alergia.component.css']
})

         ////////CLASSE COMPONENTE USADO PARA ADICIONAR UM ALERGIA/////////

export class AdicionarAlergiaComponent implements OnInit {

  grauperigos = ['ALTO', 'MÉDIO', 'BAIXO'];
  alergia:Alergia = new Alergia();

  constructor(private alergiaService:AlergiaService, private router:Router,
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.formulario = this.formBuilder.group({
      nome: [''],
      causa: [''],
      grauPerigo: ['']
    });
  }

  ////METODO RESPONSAVEL PELO CADASTRO D ALERGIA///////////

  OnSubmit(){
    this.alergia.nome = this.formulario.get('nome').value;
    this.alergia.causa =this.formulario.get('causa').value; 
    this.alergia.grauPerigo =this.formulario.get('grauPerigo').value; 

    console.log(this.alergia);
    
   this.alergiaService.createAlergia(this.alergia)
   .subscribe(data=>{alert("ALERGIA CADASTRADO COM SUCESSO...!!!");

   this.router.navigate(["listaralergia"]);
    this.formulario.reset(this.alergia);
   },
   erro => {
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

}
