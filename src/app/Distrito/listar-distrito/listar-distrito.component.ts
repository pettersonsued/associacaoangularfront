import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Distrito } from 'src/app/Model/Distrito';
import { CidadeService } from 'src/app/Service/cidade.service';
import { DistritoService } from 'src/app/Service/distrito.service';

@Component({
  selector: 'app-listar-distrito',
  templateUrl: './listar-distrito.component.html',
  styleUrls: ['./listar-distrito.component.css']
})

     ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UMA DISTRITO/////////

export class ListarDistritoComponent implements OnInit {

  distritos:Distrito[]=[];

  constructor(private distritoService:DistritoService, private cidadeService:CidadeService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

  ////////BUSCA TODOS OS  DISTRITO /////////

  Listar(){
    this.distritoService.getDistritos().subscribe(data=>{this.distritos=data;})
    this.router.navigate(["listardistrito"]);
  }

 ////////METODO DELETA SO  DISTRITO /////////

  Delete(distrito:Distrito){
    this.distritoService.deleteDistrito(distrito).subscribe(data=>{
    this.distritos = this.distritos.filter(p=>p!==distrito);})  
    alert("Distrito Deletado Com Sucesso...!!!");
  }

  ////////METODO PASSA O ID DO DISTRITO SELECIONADO NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(distrito:Distrito): void{
    localStorage.setItem("id", distrito.id.toString());
    this.router.navigate(["atualizardistrito"]); 
  }

  CadastrarDistrito(){
    this.router.navigate(["adicionardistrito"]);
  }

}
