import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarDistritoComponent } from './adicionar-distrito.component';

describe('AdicionarDistritoComponent', () => {
  let component: AdicionarDistritoComponent;
  let fixture: ComponentFixture<AdicionarDistritoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdicionarDistritoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarDistritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
