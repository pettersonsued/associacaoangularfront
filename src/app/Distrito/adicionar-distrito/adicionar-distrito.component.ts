import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Cidade } from 'src/app/Model/Cidade';
import { Distrito } from 'src/app/Model/Distrito';
import { CidadeService } from 'src/app/Service/cidade.service';
import { DistritoService } from 'src/app/Service/distrito.service';

@Component({
  selector: 'app-adicionar-distrito',
  templateUrl: './adicionar-distrito.component.html',
  styleUrls: ['./adicionar-distrito.component.css']
})

    ////////CLASSE COMPONENTE USADO PARA ADICIONAR UMA DISTRITO/////////

export class AdicionarDistritoComponent implements OnInit {

  cidades:Cidade[]=[];
  cidade:Cidade=new Cidade();
  distrito:Distrito=new Distrito(); 

  constructor(private distritoService:DistritoService, private cidadeService:CidadeService, 
              private router:Router, private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.BuscaCidades();
    this.formulario = this.formBuilder.group({
      nome: [''],
      cidade: ['']
    });
  }

   ///////RETORNA OS CIDADE PARA SELECTBOX SEREM SELECIONADO/////////////

   BuscaCidades() {
    this.cidadeService.getCidades().subscribe(data => {
      console.log(data);
      this.cidades = data;
    });
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO DISTRITO///////////

  OnSubmit(){
    this.cidade = this.formulario.get('cidade').value
    this.distrito.cidade = this.cidade;

    this.distrito.nome = this.formulario.get('nome').value;

    console.log(this.cidade);
    console.log(this.distrito);
    
   this.distritoService.createDistrito(this.distrito)
   .subscribe(data=>{alert("Distrito Cadastrado Com Sucesso...!!!");

   this.router.navigate(["listardistrito"]);
    this.formulario.reset(this.distrito);
   },
   erro => {
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

  CadastrarCidade(){
    this.router.navigate(["adicionarcidade"]);
  }

}
