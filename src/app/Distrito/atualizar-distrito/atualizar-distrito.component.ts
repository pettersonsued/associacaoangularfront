import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Cidade } from 'src/app/Model/Cidade';
import { Distrito } from 'src/app/Model/Distrito';
import { Rua } from 'src/app/Model/Rua';
import { CidadeService } from 'src/app/Service/cidade.service';
import { DistritoService } from 'src/app/Service/distrito.service';

@Component({
  selector: 'app-atualizar-distrito',
  templateUrl: './atualizar-distrito.component.html',
  styleUrls: ['./atualizar-distrito.component.css']
})

     ////////CLASSE COMPONENTE USADO PARA EDITAR UM DISTRITO/////////

export class AtualizarDistritoComponent implements OnInit {

  cidades:Cidade[]=[];
  cidade:Cidade=new Cidade();
  distrito:Distrito=new Distrito(); 

  constructor(private distritoService:DistritoService, private cidadeService:CidadeService,
              private router:Router, private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

   ////////METODO USADO PARA SETAR O DISTRITO NO FORMULARIO UM PEDIDO/////////

   Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    //console.log("aqui  "+numberValue)
    this.distritoService.getDistritoPorID(numberValue)
    .subscribe(data=>{  
      this.distrito=data;
      console.log(this.distrito)

     this.formulario = this.formBuilder.group({
        id: [this.distrito.id],
        nome: [this.distrito.nome],
        cidade: [this.distrito.cidade]
        })

        this.cidadeService.getCidades().subscribe(data => {
          this.cidades = data;
          let index = this.cidades.findIndex((existeCidade) => existeCidade.id === this.distrito.cidade.id)
          this.cidades.splice(index, 1);
          this.cidades.unshift(this.distrito.cidade);
        })
    })
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO DISTRITO///////////

  OnSubmit(){
    this.cidade = this.formulario.get('cidade').value
    console.log(" aqui "+this.distrito.id)
    this.distrito.cidade = this.cidade;

    this.distrito.nome = this.formulario.get('nome').value;

    console.log(this.cidade);
    console.log(this.distrito);
    
   this.distritoService.updateDistrito(this.distrito)
   .subscribe(data=>{alert("Disstrito Atualizado Com Sucesso...!!!");
   this.router.navigate(["listardistrito"]);
   this.formulario.reset(this.distrito);
   },
   erro => {
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })  
  }

  CadastrarCidade(){
    this.router.navigate(["adicionarcidade"]);
  }

}
