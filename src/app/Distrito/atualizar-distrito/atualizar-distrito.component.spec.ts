import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarDistritoComponent } from './atualizar-distrito.component';

describe('AtualizarDistritoComponent', () => {
  let component: AtualizarDistritoComponent;
  let fixture: ComponentFixture<AtualizarDistritoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarDistritoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarDistritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
