import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Endereco } from 'src/app/Model/Endereco';
import { Irmao } from 'src/app/Model/Irmao';
import { PessoaFisica } from 'src/app/Model/PessoaFisica';
import { Rua } from 'src/app/Model/Rua';
import { IrmaoService } from 'src/app/Service/irmao.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-atualizar-irmao',
  templateUrl: './atualizar-irmao.component.html',
  styleUrls: ['./atualizar-irmao.component.css']
})

                                          ////////COMPONENTE PARA ATUALIZAR IRMÃO/////////

export class AtualizarIrmaoComponent implements OnInit {

  sexos = ['MASCULINO', 'FEMININO', 'OUTRO'];
  ruas:Rua[]=[];
  rua:Rua=new Rua();
  endereco:Endereco = new Endereco();
  pessoa:PessoaFisica=new PessoaFisica(); 
  irmao:Irmao = new Irmao();

  constructor(private irmaoService:IrmaoService, private ruaService:RuaService,
              private router:Router, private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

  ////////METODO USADO PARA SETAR ATRIBUTOS DO IRMÃO NO FORMULÁRIO/////////

  Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.irmaoService.getIrmaoPorID(numberValue)
    .subscribe(data=>{  
      this.irmao=data; 

      this.endereco.id = this.irmao.pessoa.endereco.id;
      this.pessoa.id = this.irmao.pessoa.id
      this.irmao.id = this.irmao.id;

      this.formulario = this.formBuilder.group({
        nome: [this.irmao.pessoa.nome],
        fone: [this.irmao.pessoa.fone],
        email: [this.irmao.pessoa.email],
        rua: [this.irmao.pessoa.endereco.rua],
        cpf: [this.irmao.pessoa.cpf],
        rg: [this.irmao.pessoa.rg],
        sexo: [this.irmao.pessoa.sexo],
        referencia: [this.irmao.pessoa.endereco.referencia],
        numero:[this.irmao.pessoa.endereco.numero],
        dataNascimento: [this.irmao.pessoa.dataNascimento]
      });


        this.ruaService.getRuas().subscribe(data => {
          this.ruas = data;
          let index = this.ruas.findIndex((existeRua) => existeRua.id === this.irmao.pessoa.endereco.rua.id)
          this.ruas.splice(index, 1);
          this.ruas.unshift(this.irmao.pessoa.endereco.rua);
        })
    })
  }

  ////METODO RESPONSAVEL POR ATUALIZAR O IRMÃO///////////

  OnSubmit(){
    this.rua = this.formulario.get('rua').value

    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cpf = this.formulario.get('cpf').value
    this.pessoa.rg = this.formulario.get('rg').value
    this.pessoa.sexo = this.formulario.get('sexo').value
    this.pessoa.dataNascimento = this.formulario.get('dataNascimento').value
    this.pessoa.endereco = this.endereco;
    
    this.irmao.pessoa = this.pessoa;

    console.log(this.rua);
    console.log(this.endereco);
    console.log(this.pessoa);
    console.log(this.irmao);
    
   this.irmaoService.updateIrmao(this.irmao)
   .subscribe(data=>{alert("IRMÃO ATUALIZADO COM SUCESSO...!!!");

   this.router.navigate(["listarirmao"]);
    this.formulario.reset(this.rua);
    this.formulario.reset(this.pessoa);
    this.formulario.reset(this.endereco);
   },
   erro => {
    //alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

   CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

}
