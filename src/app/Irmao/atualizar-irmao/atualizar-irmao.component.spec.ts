import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarIrmaoComponent } from './atualizar-irmao.component';

describe('AtualizarIrmaoComponent', () => {
  let component: AtualizarIrmaoComponent;
  let fixture: ComponentFixture<AtualizarIrmaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarIrmaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarIrmaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
