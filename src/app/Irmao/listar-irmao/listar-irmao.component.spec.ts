import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarIrmaoComponent } from './listar-irmao.component';

describe('ListarIrmaoComponent', () => {
  let component: ListarIrmaoComponent;
  let fixture: ComponentFixture<ListarIrmaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarIrmaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarIrmaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
