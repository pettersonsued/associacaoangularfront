import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Irmao } from 'src/app/Model/Irmao';
import { IrmaoService } from 'src/app/Service/irmao.service';

@Component({
  selector: 'app-listar-irmao',
  templateUrl: './listar-irmao.component.html',
  styleUrls: ['./listar-irmao.component.css']
})

        ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UM IRMÃO///////// 

export class ListarIrmaoComponent implements OnInit {

  irmaos:Irmao[]=[];

  constructor(private irmaoService:IrmaoService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

  ////////BUSCA TODOS OS IRMÃOS/////////

  Listar(){
    this.irmaoService.getIrmaos().subscribe(data=>{this.irmaos=data;})
    console.log(this.irmaos.length)
    this.router.navigate(["listarirmao"]);
  }

 ////////METODO DELETA OS IRMÃOS/////////

  Delete(irmao:Irmao){
    this.irmaoService.deleteIrmao(irmao).subscribe(data=>{
    this.irmaos = this.irmaos.filter(p=>p!==irmao);})  
    alert("IRMÃO DELETADO COM SUCESSO...!!!");
  }

  ////////METODO PASSA O ID DOS IRMÃOSSELECIONADA NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(irmao:Irmao): void{
    localStorage.setItem("id", irmao.id.toString());
    this.router.navigate(["atualizarirmao"]); 
  }

  CadastrarIrmao(){
    this.router.navigate(["adicionarirmao"]);
  }

}
