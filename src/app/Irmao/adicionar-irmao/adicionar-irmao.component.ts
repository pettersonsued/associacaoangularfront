import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Endereco } from 'src/app/Model/Endereco';
import { Irmao } from 'src/app/Model/Irmao';
import { PessoaFisica } from 'src/app/Model/PessoaFisica';
import { Rua } from 'src/app/Model/Rua';
import { IrmaoService } from 'src/app/Service/irmao.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-adicionar-irmao',
  templateUrl: './adicionar-irmao.component.html',
  styleUrls: ['./adicionar-irmao.component.css']
})

         ////////CLASSE COMPONENTE USADO PARA ADICIONAR UMA IRMÃO////////

export class AdicionarIrmaoComponent implements OnInit {

  sexos = ['MASCULINO', 'FEMININO', 'OUTRO'];
  ruas:Rua[]=[];
  rua:Rua=new Rua();
  endereco:Endereco = new Endereco();
  pessoa:PessoaFisica=new PessoaFisica(); 
  irmao:Irmao =new Irmao();

  constructor(private irmaoService:IrmaoService, private ruaService:RuaService,
              private router:Router, private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.BuscaRuas();
    this.formulario = this.formBuilder.group({
      nome: [''],
      fone: [''],
      email: [''],
      rua: [''],
      cpf: [''],
      rg: [''],
      sexo: [''],
      referencia: [''],
      numero:[''],
      dataNascimento: ['']
    });
  }

  BuscaRuas() {
    this.ruaService.getRuas().subscribe(data => {
      console.log(data);
      this.ruas = data;
    });
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO IRMÃO///////////

  OnSubmit(){
    this.rua = this.formulario.get('rua').value

    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cpf = this.formulario.get('cpf').value
    this.pessoa.rg = this.formulario.get('rg').value
    this.pessoa.sexo = this.formulario.get('sexo').value
    this.pessoa.dataNascimento = this.formulario.get('dataNascimento').value
    this.pessoa.endereco = this.endereco;
    
    this.irmao.pessoa = this.pessoa;

    console.log(this.rua);
    console.log(this.endereco);
    console.log(this.pessoa);
    console.log(this.irmao);
    
   this.irmaoService.createIrmao(this.irmao)
   .subscribe(data=>{alert("IRMÃO CADASTRADO COM SUCESSO...!!!");

   this.router.navigate(["listarirmao"]);
    this.formulario.reset(this.irmao);
   },
   erro => {
    alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

   CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

}
