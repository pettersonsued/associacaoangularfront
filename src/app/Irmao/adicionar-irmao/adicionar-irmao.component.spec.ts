import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarIrmaoComponent } from './adicionar-irmao.component';

describe('AdicionarIrmaoComponent', () => {
  let component: AdicionarIrmaoComponent;
  let fixture: ComponentFixture<AdicionarIrmaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdicionarIrmaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarIrmaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
