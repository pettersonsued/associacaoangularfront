import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './menu/menu.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ListarRuaComponent } from './Rua/listar-rua/listar-rua.component';
import { AdicionarRuaComponent } from './Rua/adicionar-rua/adicionar-rua.component';
import { AtualizarRuaComponent } from './Rua/atualizar-rua/atualizar-rua.component';
import { ListarDistritoComponent } from './Distrito/listar-distrito/listar-distrito.component';
import { AdicionarDistritoComponent } from './Distrito/adicionar-distrito/adicionar-distrito.component';
import { AtualizarDistritoComponent } from './Distrito/atualizar-distrito/atualizar-distrito.component';
import { ListarCidadeComponent } from './Cidade/listar-cidade/listar-cidade.component';
import { AdicionarCidadeComponent } from './Cidade/adicionar-cidade/adicionar-cidade.component';
import { AtualizarCidadeComponent } from './Cidade/atualizar-cidade/atualizar-cidade.component';
import { EnderecoService } from './Service/endereco.service';
import { RuaService } from './Service/rua.service';
import { DistritoService } from './Service/distrito.service';
import { CidadeService } from './Service/cidade.service';
import { AdicionarEscolaComponent } from './Escola/adicionar-escola/adicionar-escola.component';
import { AtualizarEscolaComponent } from './Escola/atualizar-escola/atualizar-escola.component';
import { ListarEscolaComponent } from './Escola/listar-escola/listar-escola.component';
import { EscolaService } from './Service/escola.service';
import { ListarCartorioComponent } from './Cartorio/listar-cartorio/listar-cartorio.component';
import { AdicionarCartorioComponent } from './Cartorio/adicionar-cartorio/adicionar-cartorio.component';
import { AtualizarCartorioComponent } from './Cartorio/atualizar-cartorio/atualizar-cartorio.component';
import { CartorioService } from './Service/cartorio.service';
import { AdicionarPaiComponent } from './Pai/adicionar-pai/adicionar-pai.component';
import { AtualizarPaiComponent } from './Pai/atualizar-pai/atualizar-pai.component';
import { ListarPaiComponent } from './Pai/listar-pai/listar-pai.component';
import { ListarMaeComponent } from './Mae/listar-mae/listar-mae.component';
import { AdicionarMaeComponent } from './Mae/adicionar-mae/adicionar-mae.component';
import { AtualizarMaeComponent } from './Mae/atualizar-mae/atualizar-mae.component';
import { PaiService } from './Service/pai.service';
import { MaeService } from './Service/mae.service';
import { ListarProblesaudeComponent } from './Problesaude/listar-problesaude/listar-problesaude.component';
import { AdicionarProblesaudeComponent } from './Problesaude/adicionar-problesaude/adicionar-problesaude.component';
import { AtualizarProblesaudeComponent } from './Problesaude/atualizar-problesaude/atualizar-problesaude.component';
import { ProblesaudeService } from './Service/problesaude.service';
import { AtualizarAlergiaComponent } from './Alergia/atualizar-alergia/atualizar-alergia.component';
import { AdicionarAlergiaComponent } from './Alergia/adicionar-alergia/adicionar-alergia.component';
import { ListarAlergiaComponent } from './Alergia/listar-alergia/listar-alergia.component';
import { AlergiaService } from './Service/alergia.service';
import { ListarIrmaoComponent } from './Irmao/listar-irmao/listar-irmao.component';
import { AdicionarIrmaoComponent } from './Irmao/adicionar-irmao/adicionar-irmao.component';
import { AtualizarIrmaoComponent } from './Irmao/atualizar-irmao/atualizar-irmao.component';
import { IrmaoService } from './Service/irmao.service';
import { AtualizarAlunoComponent } from './Aluno/atualizar-aluno/atualizar-aluno.component';
import { AdicionarAlunoComponent } from './Aluno/adicionar-aluno/adicionar-aluno.component';
import { ListarAlunoComponent } from './Aluno/listar-aluno/listar-aluno.component';
import { AlunoService } from './Service/aluno.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DashboardComponent,
    ListarRuaComponent,
    AdicionarRuaComponent,
    AtualizarRuaComponent,
    ListarDistritoComponent,
    AdicionarDistritoComponent,
    AtualizarDistritoComponent,
    ListarCidadeComponent,
    AdicionarCidadeComponent,
    AtualizarCidadeComponent,
    AdicionarEscolaComponent,
    AtualizarEscolaComponent,
    ListarEscolaComponent,
    AdicionarEscolaComponent,
    AtualizarEscolaComponent,
    ListarEscolaComponent,
    ListarCartorioComponent,
    AdicionarCartorioComponent,
    AtualizarCartorioComponent,
    AdicionarPaiComponent,
    AtualizarPaiComponent,
    ListarPaiComponent,
    ListarMaeComponent,
    AdicionarMaeComponent,
    AtualizarMaeComponent,
    ListarProblesaudeComponent,
    AdicionarProblesaudeComponent,
    AtualizarProblesaudeComponent,
    AtualizarAlergiaComponent,
    AdicionarAlergiaComponent,
    ListarAlergiaComponent,
    ListarIrmaoComponent,
    AdicionarIrmaoComponent,
    AtualizarIrmaoComponent,
    AtualizarAlunoComponent,
    AdicionarAlunoComponent,
    ListarAlunoComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule
  ],
  providers: [
    EnderecoService,
    RuaService,
    DistritoService,
    CidadeService,
    EscolaService,
    CartorioService,
    PaiService,
    MaeService,
    ProblesaudeService,
    AlergiaService,
    IrmaoService,
    AlunoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
