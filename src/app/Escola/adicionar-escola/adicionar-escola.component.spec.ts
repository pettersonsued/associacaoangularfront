import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarEscolaComponent } from './adicionar-escola.component';

describe('AdicionarEscolaComponent', () => {
  let component: AdicionarEscolaComponent;
  let fixture: ComponentFixture<AdicionarEscolaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdicionarEscolaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarEscolaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
