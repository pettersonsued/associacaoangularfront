import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Endereco } from 'src/app/Model/Endereco';
import { Escola } from 'src/app/Model/Escola';
import { PessoaJuridica } from 'src/app/Model/PessoaJuridica';
import { Rua } from 'src/app/Model/Rua';
import { EscolaService } from 'src/app/Service/escola.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-adicionar-escola',
  templateUrl: './adicionar-escola.component.html',
  styleUrls: ['./adicionar-escola.component.css']
})

      ////////CLASSE COMPONENTE USADO PARA ADICIONAR UMA ESCOLA/////////

export class AdicionarEscolaComponent implements OnInit {

  ruas:Rua[]=[];
  rua:Rua=new Rua();
  endereco:Endereco = new Endereco();
  pessoa:PessoaJuridica=new PessoaJuridica(); 
  escola:Escola=new Escola();

  constructor(private escolaService:EscolaService, private router:Router, private formBuilder:FormBuilder,
              private ruaService:RuaService) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.BuscaRuas();
    this.formulario = this.formBuilder.group({
      nome: [''],
      fone: [''],
      email: [''],
      rua: [''],
      cnpj: [''],
      razao: [''],
      referencia: [''],
      numero:[''],
      dataFundacao: ['']
    });
  }


  BuscaRuas() {
    this.ruaService.getRuas().subscribe(data => {
      console.log(data);
      this.ruas = data;
    });
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO ESCOLA///////////

  OnSubmit(){
    this.rua = this.formulario.get('rua').value

    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cnpj = this.formulario.get('cnpj').value
    this.pessoa.razao = this.formulario.get('razao').value
    this.pessoa.dataFundacao = this.formulario.get('dataFundacao').value
    this.pessoa.endereco = this.endereco;
    
    this.escola.pessoa = this.pessoa;

    console.log(this.rua);
    console.log(this.endereco);
    console.log(this.pessoa);
    console.log(this.escola);
    
   this.escolaService.createEscola(this.escola)
   .subscribe(data=>{alert("Escola Cadastrado Com Sucesso...!!!");

   this.router.navigate(["listarescola"]);
    this.formulario.reset(this.escola);
   },
   erro => {
    alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

   CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

}
