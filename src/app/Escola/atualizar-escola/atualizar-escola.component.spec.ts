import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarEscolaComponent } from './atualizar-escola.component';

describe('AtualizarEscolaComponent', () => {
  let component: AtualizarEscolaComponent;
  let fixture: ComponentFixture<AtualizarEscolaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarEscolaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarEscolaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
