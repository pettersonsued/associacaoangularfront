import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Endereco } from 'src/app/Model/Endereco';
import { Escola } from 'src/app/Model/Escola';
import { PessoaJuridica } from 'src/app/Model/PessoaJuridica';
import { Rua } from 'src/app/Model/Rua';
import { EscolaService } from 'src/app/Service/escola.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-atualizar-escola',
  templateUrl: './atualizar-escola.component.html',
  styleUrls: ['./atualizar-escola.component.css']
})

       ////////CLASSE COMPONENTE USADO PARA ATUALIZAR UMA ESCOLA/////////

export class AtualizarEscolaComponent implements OnInit {


  ruas:Rua[]=[];
  rua:Rua=new Rua();
  endereco:Endereco = new Endereco();
  pessoa:PessoaJuridica=new PessoaJuridica(); 
  escola:Escola=new Escola();

  constructor(private escolaService:EscolaService, private ruaService:RuaService, private router:Router,
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

  ////////METODO USADO OARA SETAR O RUA NO FORMULARIO UM ESCOLA/////////

  Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.escolaService.getEscolaPorID(numberValue)
    .subscribe(data=>{  
      this.escola=data; 

      this.endereco.id = this.escola.pessoa.endereco.id;
      this.pessoa.id = this.escola.pessoa.id
      this.escola.id = this.escola.id;

      this.formulario = this.formBuilder.group({
        nome: [this.escola.pessoa.nome],
        fone: [this.escola.pessoa.fone],
        email: [this.escola.pessoa.email],
        rua: [this.escola.pessoa.endereco.rua],
        cnpj: [this.escola.pessoa.cnpj],
        razao: [this.escola.pessoa.razao],
        referencia: [this.escola.pessoa.endereco.referencia],
        numero:[this.escola.pessoa.endereco.numero],
        dataFundacao: [this.escola.pessoa.dataFundacao]
      });


        this.ruaService.getRuas().subscribe(data => {
          this.ruas = data;
          let index = this.ruas.findIndex((existeRua) => existeRua.id === this.escola.pessoa.endereco.rua.id)
          this.ruas.splice(index, 1);
          this.ruas.unshift(this.escola.pessoa.endereco.rua);
        })
    })
  }

   ////METODO RESPONSAVEL PELO CADASTRO DA ESCOLA///////////

   OnSubmit(){
    this.rua = this.formulario.get('rua').value

    //this.endereco.id = this.endereco.id;
    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    //this.pessoa.id = this.pessoa.id;
    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cnpj = this.formulario.get('cnpj').value
    this.pessoa.razao = this.formulario.get('razao').value
    this.pessoa.dataFundacao = this.formulario.get('dataFundacao').value
    this.pessoa.endereco = this.endereco;
    
    //this.pessoa.id = this.pessoa.id;
    this.escola.pessoa = this.pessoa;
    //this.pessoa.endereco.id = this.endereco.id;

    console.log(this.rua);
    console.log(this.endereco);
    console.log(this.pessoa);
    console.log(this.escola);
    
   this.escolaService.updateEscola(this.escola)
   .subscribe(data=>{alert("Escola Atualizado Com Sucesso...!!!");

   this.router.navigate(["listarescola"]);
    this.formulario.reset(this.rua);
    this.formulario.reset(this.pessoa);
    this.formulario.reset(this.endereco);
   },
   erro => {
    //alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

   CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

}
