import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Escola } from 'src/app/Model/Escola';
import { EscolaService } from 'src/app/Service/escola.service';

@Component({
  selector: 'app-listar-escola',
  templateUrl: './listar-escola.component.html',
  styleUrls: ['./listar-escola.component.css']
})

     ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UMA ESCOLAS/////////

export class ListarEscolaComponent implements OnInit {

  escolas:Escola[]=[];

  constructor(private escolaService:EscolaService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

  ////////BUSCA TODOS OS  ESCOLA /////////

  Listar(){
    this.escolaService.getEscolas().subscribe(data=>{this.escolas=data;})
    console.log(this.escolas.length)
    this.router.navigate(["listarescola"]);
  }

 ////////METODO DELETA SO  ESCOLA /////////

  Delete(escola:Escola){
    this.escolaService.deleteEscola(escola).subscribe(data=>{
    this.escolas = this.escolas.filter(p=>p!==escola);})  
    alert("Escola Deletado Com Sucesso...!!!");
  }

  ////////METODO PASSA O ID DO ESCOLA SELECIONADO NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(escola:Escola): void{
    localStorage.setItem("id", escola.id.toString());
    this.router.navigate(["atualizarescola"]); 
  }

  CadastrarEscola(){
    this.router.navigate(["adicionarescola"]);
  }

}
