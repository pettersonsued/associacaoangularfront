import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarCartorioComponent } from './atualizar-cartorio.component';

describe('AtualizarCartorioComponent', () => {
  let component: AtualizarCartorioComponent;
  let fixture: ComponentFixture<AtualizarCartorioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarCartorioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarCartorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
