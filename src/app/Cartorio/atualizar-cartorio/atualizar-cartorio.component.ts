import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Cartorio } from 'src/app/Model/Cartorio';
import { Endereco } from 'src/app/Model/Endereco';
import { PessoaJuridica } from 'src/app/Model/PessoaJuridica';
import { Rua } from 'src/app/Model/Rua';
import { CartorioService } from 'src/app/Service/cartorio.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-atualizar-cartorio',
  templateUrl: './atualizar-cartorio.component.html',
  styleUrls: ['./atualizar-cartorio.component.css']
})

            ////////CLASSE COMPONENTE USADO PARA ATUALIZAR UMA CARTÓRIO/////////
 
export class AtualizarCartorioComponent implements OnInit {


  ruas:Rua[]=[];
  rua:Rua=new Rua();
  endereco:Endereco = new Endereco();
  pessoa:PessoaJuridica=new PessoaJuridica(); 
  cartorio:Cartorio=new Cartorio();

  constructor(private cartorioService:CartorioService, private ruaService:RuaService , private router:Router, 
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

   ////////METODO USADO OARA SETAR O RUA NO FORMULARIO UM ESCOLA/////////

   Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.cartorioService.getCartorioPorID(numberValue)
    .subscribe(data=>{  
      this.cartorio=data; 

      this.endereco.id = this.cartorio.pessoa.endereco.id;
      this.pessoa.id = this.cartorio.pessoa.id
      this.cartorio.id = this.cartorio.id;

      //console.log(this.escola.pessoa.endereco.rua.id);
    //console.log(this.escola.pessoa.endereco.id);
    //console.log(this.escola.pessoa.id);
    //console.log(this.escola.id);


      this.formulario = this.formBuilder.group({
        nome: [this.cartorio.pessoa.nome],
        fone: [this.cartorio.pessoa.fone],
        email: [this.cartorio.pessoa.email],
        rua: [this.cartorio.pessoa.endereco.rua],
        cnpj: [this.cartorio.pessoa.cnpj],
        razao: [this.cartorio.pessoa.razao],
        referencia: [this.cartorio.pessoa.endereco.referencia],
        numero:[this.cartorio.pessoa.endereco.numero],
        dataFundacao: [this.cartorio.pessoa.dataFundacao]
      });


        this.ruaService.getRuas().subscribe(data => {
          this.ruas = data;
          let index = this.ruas.findIndex((existeRua) => existeRua.id === this.cartorio.pessoa.endereco.rua.id)
          this.ruas.splice(index, 1);
          this.ruas.unshift(this.cartorio.pessoa.endereco.rua);
        })
    })
  }


   ////METODO RESPONSAVEL PELO CADASTRO DA CARTÓRIO///////////

   OnSubmit(){
    this.rua = this.formulario.get('rua').value

    //this.endereco.id = this.endereco.id;
    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    //this.pessoa.id = this.pessoa.id;
    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cnpj = this.formulario.get('cnpj').value
    this.pessoa.razao = this.formulario.get('razao').value
    this.pessoa.dataFundacao = this.formulario.get('dataFundacao').value
    this.pessoa.endereco = this.endereco;
    
    //this.pessoa.id = this.pessoa.id;
    this.cartorio.pessoa = this.pessoa;
    //this.pessoa.endereco.id = this.endereco.id;

    console.log(this.rua);
    console.log(this.endereco);
    console.log(this.pessoa);
    console.log(this.cartorio);
    
   this.cartorioService.updateCartorio(this.cartorio)
   .subscribe(data=>{alert("CARTÓRIO ATUALIZADO COM SUCESSO...!!!");

   this.router.navigate(["listarcartorio"]);
    this.formulario.reset(this.rua);
    this.formulario.reset(this.pessoa);
    this.formulario.reset(this.endereco);
   },
   erro => {
    //alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

  CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

}
