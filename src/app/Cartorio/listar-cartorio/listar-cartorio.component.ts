import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cartorio } from 'src/app/Model/Cartorio';
import { CartorioService } from 'src/app/Service/cartorio.service';

@Component({
  selector: 'app-listar-cartorio',
  templateUrl: './listar-cartorio.component.html',
  styleUrls: ['./listar-cartorio.component.css']
})

                     ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UMA CARTÓRIOS/////////

export class ListarCartorioComponent implements OnInit {

 cartorios:Cartorio[]=[];

  constructor(private cartorioService:CartorioService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

  ////////BUSCA TODOS OS  CARTORIO /////////

  Listar(){
    this.cartorioService.getCartorios().subscribe(data=>{this.cartorios=data;})
    console.log(this.cartorios.length)
    this.router.navigate(["listarcartorio"]);
  }

 ////////METODO DELETA SO  CARTORIO/////////

  Delete(cartorio:Cartorio){
    this.cartorioService.deleteCartorio(cartorio).subscribe(data=>{
    this.cartorios = this.cartorios.filter(p=>p!==cartorio);})  
    alert("CARTÓRIO DELETADO COM SUCESSO...!!!");
  }

  ////////METODO PASSA O ID DO CARTORIO SELECIONADO NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(cartorio:Cartorio): void{
    localStorage.setItem("id", cartorio.id.toString());
    this.router.navigate(["atualizarcartorio"]); 
  }

  CadastrarCartorio(){
    this.router.navigate(["adicionarcartorio"]);
  }

}
