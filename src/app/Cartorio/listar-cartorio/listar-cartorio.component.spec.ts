import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarCartorioComponent } from './listar-cartorio.component';

describe('ListarCartorioComponent', () => {
  let component: ListarCartorioComponent;
  let fixture: ComponentFixture<ListarCartorioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarCartorioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarCartorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
