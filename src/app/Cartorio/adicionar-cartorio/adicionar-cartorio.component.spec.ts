import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarCartorioComponent } from './adicionar-cartorio.component';

describe('AdicionarCartorioComponent', () => {
  let component: AdicionarCartorioComponent;
  let fixture: ComponentFixture<AdicionarCartorioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdicionarCartorioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarCartorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
