import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Cartorio } from 'src/app/Model/Cartorio';
import { Endereco } from 'src/app/Model/Endereco';
import { PessoaJuridica } from 'src/app/Model/PessoaJuridica';
import { Rua } from 'src/app/Model/Rua';
import { CartorioService } from 'src/app/Service/cartorio.service';
import { RuaService } from 'src/app/Service/rua.service';

@Component({
  selector: 'app-adicionar-cartorio',
  templateUrl: './adicionar-cartorio.component.html',
  styleUrls: ['./adicionar-cartorio.component.css']
})

                   
      ////////CLASSE COMPONENTE USADO PARA ADICIONAR UM CARTÓRIO/////////

export class AdicionarCartorioComponent implements OnInit {

  ruas:Rua[]=[];
  rua:Rua=new Rua();
  endereco:Endereco = new Endereco();
  pessoa:PessoaJuridica=new PessoaJuridica(); 
  cartorio:Cartorio=new Cartorio();

  constructor(private cartorioService:CartorioService, private router:Router, private ruaService:RuaService,
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.BuscaRuas();
    this.formulario = this.formBuilder.group({
      nome: [''],
      fone: [''],
      email: [''],
      rua: [''],
      cnpj: [''],
      razao: [''],
      referencia: [''],
      numero:[''],
      dataFundacao: ['']
    });
  }

  BuscaRuas() {
    this.ruaService.getRuas().subscribe(data => {
      console.log(data);
      this.ruas = data;
    });
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO RUA///////////

  OnSubmit(){
    this.rua = this.formulario.get('rua').value

    this.endereco.rua = this.rua;
    this.endereco.referencia = this.formulario.get('referencia').value
    this.endereco.numero = this.formulario.get('numero').value

    this.pessoa.nome = this.formulario.get('nome').value
    this.pessoa.fone = this.formulario.get('fone').value
    this.pessoa.email = this.formulario.get('email').value
    this.pessoa.cnpj = this.formulario.get('cnpj').value
    this.pessoa.razao = this.formulario.get('razao').value
    this.pessoa.dataFundacao = this.formulario.get('dataFundacao').value
    this.pessoa.endereco = this.endereco;
    
    this.cartorio.pessoa = this.pessoa;

    console.log(this.rua);
    console.log(this.endereco);
    console.log(this.pessoa);
    console.log(this.cartorio);
    
   this.cartorioService.createCartorio(this.cartorio)
   .subscribe(data=>{alert("CARTÓRIO CADASTRADO COM SUCESSO...!!!");

   this.router.navigate(["listarcartorio"]);
    this.formulario.reset(this.cartorio);
   },
   erro => {
    alert(erro);
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

  CadastrarRua(){
    this.router.navigate(["adicionarrua"]);
  }

}
