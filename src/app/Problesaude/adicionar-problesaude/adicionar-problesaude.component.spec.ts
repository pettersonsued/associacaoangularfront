import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarProblesaudeComponent } from './adicionar-problesaude.component';

describe('AdicionarProblesaudeComponent', () => {
  let component: AdicionarProblesaudeComponent;
  let fixture: ComponentFixture<AdicionarProblesaudeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdicionarProblesaudeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarProblesaudeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
