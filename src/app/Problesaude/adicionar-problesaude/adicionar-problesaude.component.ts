import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ProblemaSaude } from 'src/app/Model/ProblemaSaude';
import { ProblesaudeService } from 'src/app/Service/problesaude.service';

@Component({
  selector: 'app-adicionar-problesaude',
  templateUrl: './adicionar-problesaude.component.html',
  styleUrls: ['./adicionar-problesaude.component.css']
})

      ////////CLASSE COMPONENTE USADO PARA ADICIONAR UM PROBLEMA SAÚDE/////////

export class AdicionarProblesaudeComponent implements OnInit {

  grauperigos = ['ALTO', 'MÉDIO', 'BAIXO'];
  problesaude:ProblemaSaude = new ProblemaSaude(); 

  constructor(private problesaudeService:ProblesaudeService, private router:Router,
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.formulario = this.formBuilder.group({
      nome: [''],
      causa: [''],
      grauPerigo: ['']
    });
  }

  ////METODO RESPONSAVEL PELO CADASTRO DO RUA///////////

  OnSubmit(){
    this.problesaude.nome = this.formulario.get('nome').value;
    this.problesaude.causa =this.formulario.get('causa').value; 
    this.problesaude.grauPerigo =this.formulario.get('grauPerigo').value; 

    console.log(this.problesaude);
    
   this.problesaudeService.createProblesaude(this.problesaude)
   .subscribe(data=>{alert("PROBLEMA SAÚDE CADASTRADO COM SUCESSO...!!!");

   this.router.navigate(["listarproblesaude"]);
    this.formulario.reset(this.problesaude);
   },
   erro => {
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

}
