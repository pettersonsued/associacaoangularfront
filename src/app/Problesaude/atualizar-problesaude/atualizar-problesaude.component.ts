import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ProblemaSaude } from 'src/app/Model/ProblemaSaude';
import { ProblesaudeService } from 'src/app/Service/problesaude.service';

@Component({
  selector: 'app-atualizar-problesaude',
  templateUrl: './atualizar-problesaude.component.html',
  styleUrls: ['./atualizar-problesaude.component.css']
})

           ////////CLASSE COMPONENTE USADO PARA ATUALIZA UM PROBLEMA SAÚDE/////////

export class AtualizarProblesaudeComponent implements OnInit {

  grauperigos = ['ALTO', 'MÉDIO', 'BAIXO'];
  problesaude:ProblemaSaude = new ProblemaSaude(); 

  constructor(private problesaudeService:ProblesaudeService, private router:Router,
              private formBuilder:FormBuilder) { }

              formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

  ////////METODO USADO OARA SETAR O RUA NO FORMULARIO UM PEDIDO/////////

  Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.problesaudeService.getProblesaudePorID(numberValue)
    .subscribe(data=>{  
      this.problesaude=data;

     this.formulario = this.formBuilder.group({
        id: [this.problesaude.id],
        nome: [this.problesaude.nome],
        causa: [this.problesaude.causa],
        grauPerigo: [this.problesaude.grauPerigo]
        })
    })
  }

  ////METODO RESPONSAVEL POR ATUALIZAR O PROBLEMA SAÚDE///////////

  OnSubmit(){
    this.problesaude.nome = this.formulario.get('nome').value;
    this.problesaude.causa = this.formulario.get('causa').value;
    this.problesaude.grauPerigo = this.formulario.get('grauPerigo').value; 
    
   this.problesaudeService.updateProblesaude(this.problesaude)
   .subscribe(data=>{alert("PROBLEMA SAÚDE ATUALIZADO COM SUCESSO...!!!");
   this.router.navigate(["listarproblesaude"]);
   this.formulario.reset(this.problesaude);
   },
   erro => {
    if(erro.status == 400) {
      alert(erro.error.titulo);
    }
  })  
  }

}
