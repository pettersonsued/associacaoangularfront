import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarProblesaudeComponent } from './atualizar-problesaude.component';

describe('AtualizarProblesaudeComponent', () => {
  let component: AtualizarProblesaudeComponent;
  let fixture: ComponentFixture<AtualizarProblesaudeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtualizarProblesaudeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarProblesaudeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
