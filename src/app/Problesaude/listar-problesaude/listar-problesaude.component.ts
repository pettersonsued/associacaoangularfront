import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProblemaSaude } from 'src/app/Model/ProblemaSaude';
import { ProblesaudeService } from 'src/app/Service/problesaude.service';

@Component({
  selector: 'app-listar-problesaude',
  templateUrl: './listar-problesaude.component.html',
  styleUrls: ['./listar-problesaude.component.css']
})

        ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UM PROBLEMA SAÚDE/////////

export class ListarProblesaudeComponent implements OnInit {

  problesaudes:ProblemaSaude[]=[];

  constructor(private problesaudeService:ProblesaudeService, private router:Router) { }

  ngOnInit(): void {
    this.Listar();
  }

  ////////BUSCA TODOS OS PROBLEMA SAÚDE /////////

  Listar(){
    this.problesaudeService.getProblesaudes().subscribe(data=>{this.problesaudes=data;})
    this.router.navigate(["listarproblesaude"]);
  }

 ////////METODO DELETA PROBLEMA SAÚDE /////////

  Delete(problesaude:ProblemaSaude){
    this.problesaudeService.deleteProblesaude(problesaude).subscribe(data=>{
    this.problesaudes = this.problesaudes.filter(p=>p!==problesaude);})  
    alert("PROBLEMA SAÚDE DELETADO COM SUCESSO...!!!");
  }

  ////////METODO PASSA O ID DO PROBLEMA SAÚDE SELECIONADO NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(problesaude:ProblemaSaude): void{
    localStorage.setItem("id", problesaude.id.toString());
    this.router.navigate(["atualizarproblesaude"]); 
  }

  CadastrarProblesaude(){
    this.router.navigate(["adicionarproblesaude"]);
  }

}
