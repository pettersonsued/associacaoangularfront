import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarProblesaudeComponent } from './listar-problesaude.component';

describe('ListarProblesaudeComponent', () => {
  let component: ListarProblesaudeComponent;
  let fixture: ComponentFixture<ListarProblesaudeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarProblesaudeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarProblesaudeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
