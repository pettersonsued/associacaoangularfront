import { Rua } from "./Rua";

export class Endereco{
    id:number=0;
    rua:Rua = new Rua;
    referencia:String ="";
    numero:number=0;
}