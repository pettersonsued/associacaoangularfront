import { Alergia } from "./Alergia";
import { Cidade } from "./Cidade";
import { Escola } from "./Escola";
import { Irmao} from "./Irmao";
import { Mae } from "./Mae";
import { Pai } from "./Pai";
import { PessoaFisica } from "./PessoaFisica";
import { ProblemaSaude } from "./ProblemaSaude";
import { Registro } from "./Registro";

export class Aluno {

    id:number=0;
    matriculaAssociacao:Number=0;
    dataMatriculaAssociacao:Date = new Date();
    racaCor:String="";
    matriculaEscola:Number=0;
    serie:String="";
    turno:String="";
    situacaoAssociacao:String="";
    bolsa:String="";
    pessoa:PessoaFisica = new PessoaFisica();
    orgaoExpedidorRg:String="";
    dataExpedicaoRg:Date = new Date();
    pai:Pai = new Pai();
    mae:Mae = new Mae();
    cidade:Cidade = new Cidade();
    registro:Registro = new Registro();
    escola:Escola = new Escola();
    irmaos:Irmao[]=[];
    problemas:ProblemaSaude[]=[];
    alergias:Alergia[]=[];
}