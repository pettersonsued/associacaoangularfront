import { Cidade } from "./Cidade";
import { Rua } from "./Rua";

export class Distrito {
    id:number=0;
    nome:String ="";
    cidade:Cidade = new Cidade;
    ruas:Rua[]=[];
}