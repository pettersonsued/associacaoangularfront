import { Aluno } from "./Aluno";
import { Distrito } from "./Distrito";

export class Cidade {
    id:number=0;
    nome:String ="";
    estado:String ="";
    distritos:Distrito[]=[];
    alunos:Aluno[]=[];
}