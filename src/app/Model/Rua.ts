import { Distrito } from "./Distrito";
import { Endereco } from "./Endereco";

export class Rua{
    id:number=0;
    logradouro:String ="";
    cep:String ="";
    distrito:Distrito = new Distrito;
    enderecos:Endereco[]=[];
}