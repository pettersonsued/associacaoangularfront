import { Aluno } from "./Aluno";
import { PessoaJuridica } from "./PessoaJuridica";

export class Escola{
    id:number=0;
    pessoa:PessoaJuridica = new PessoaJuridica();
    alunos:Aluno[]=[];
}