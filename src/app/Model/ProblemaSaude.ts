import { Aluno } from "./Aluno";

export class ProblemaSaude {
    id:number=0;
    nome:String="";
    causa:String="";
    grauPerigo:String="";
    alunos:Aluno[]=[];
}