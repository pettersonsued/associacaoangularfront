import { Endereco } from "./Endereco";

export class PessoaFisica {
    id:number=0;
    nome:String ="";
    fone:String ="";
    email:String ="";
    endereco:Endereco = new Endereco();
    cpf:String ="";
    rg:String ="";
    sexo:String ="";
    dataNascimento: Date = new Date();
}