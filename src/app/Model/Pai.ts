import { Aluno } from "./Aluno";
import { PessoaFisica } from "./PessoaFisica";

export class Pai {
    id:number=0;
    escolaridade:String="";
    profissao:String=""
    local_trabalho:String="";
    pessoa:PessoaFisica = new PessoaFisica();
    list_alunos:Aluno[]=[];
}