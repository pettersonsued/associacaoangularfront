import { Cartorio } from "./Cartorio";

export class Registro {
    id:number=0;
    numeroLivro:number=0;
    numeroFolha:number=0;
    numeroTermo:number=0;
    cartorio:Cartorio = new Cartorio();
}