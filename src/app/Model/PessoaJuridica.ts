import { Endereco } from "./Endereco";

export class PessoaJuridica {
    id:number=0;
    nome:String ="";
    fone:String ="";
    email:String ="";
    endereco:Endereco = new Endereco();
    cnpj:String ="";
    razao:String ="";
    dataFundacao: Date = new Date();
}