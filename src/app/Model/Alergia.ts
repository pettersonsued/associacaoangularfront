import { Aluno } from "./Aluno";

export class Alergia {
    id:number=0;
    nome:String="";
    causa:String="";
    grauPerigo:String="";
    alunos:Aluno[]=[];
}