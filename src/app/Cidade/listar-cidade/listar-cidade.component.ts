import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cidade } from 'src/app/Model/Cidade';
import { CidadeService } from 'src/app/Service/cidade.service';

@Component({
  selector: 'app-listar-cidade',
  templateUrl: './listar-cidade.component.html',
  styleUrls: ['./listar-cidade.component.css']
})

     ////////CLASSE COMPONENTE USADO PARA LISTAR E DELETAR UMA CIDADE/////////

export class ListarCidadeComponent implements OnInit {

  cidades:Cidade[]=[];

  constructor(private cidadeService:CidadeService, private router:Router) { }

  ngOnInit(): void {
  this.Listar();
  }

  ////////BUSCA TODOS AS CIDADES /////////

  Listar(){
    this.cidadeService.getCidades().subscribe(data=>{this.cidades=data;})
    this.router.navigate(["listarcidade"]);
  }

 ////////METODO DELETA AS CIDADES  /////////

  Delete(cidade:Cidade){
    this.cidadeService.deleteCidade(cidade).subscribe(data=>{
    this.cidades = this.cidades.filter(p=>p!==cidade);})  
    alert("Cidade Deletado Com Sucesso...!!!");
  }

  ////////METODO PASSA O ID DO CIDADE SELECIONADO NA TABELA LISTA PARA ATUALIZAR/////////

  Atualizar(cidade:Cidade): void{
    localStorage.setItem("id", cidade.id.toString());
    this.router.navigate(["atualizarcidade"]); 
  }

  CadastrarCidade(){
    this.router.navigate(["adicionarcidade"]);
  }

  CadastrarDistrito(){
    this.router.navigate(["adicionardistrito"]);
  }
}
