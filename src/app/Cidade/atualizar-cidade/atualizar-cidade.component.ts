import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Cidade } from 'src/app/Model/Cidade';
import { CidadeService } from 'src/app/Service/cidade.service';

@Component({
  selector: 'app-atualizar-cidade',
  templateUrl: './atualizar-cidade.component.html',
  styleUrls: ['./atualizar-cidade.component.css']
})

      ////////CLASSE COMPONENTE USADO PARA EDITAR UMa cidade/////////

export class AtualizarCidadeComponent implements OnInit {

  estados:String[]=[];
  estado:String ="";
  cidade:Cidade=new Cidade(); 

  constructor(private cidadeService:CidadeService, private router:Router, private formBuilder:FormBuilder) { }

             formulario: any = FormGroup;

  ngOnInit(): void {
    this.Atualizar();
  }

   ////////METODO USADO PARA SETAR A CIDADE NO FORMULARIO UM PEDIDO/////////

   Atualizar(){
    let id= localStorage.getItem("id");
    var numberValue = Number(id);
    this.cidadeService.getCidadePorID(numberValue)
    .subscribe(data=>{  
      this.cidade=data;

     this.formulario = this.formBuilder.group({
        id: [this.cidade.id],
        nome: [this.cidade.nome],
        estado: [this.cidade.estado]
        })

          this.estados = this.getEstados();
          let index = this.estados.findIndex((existeEstado) => existeEstado === this.cidade.estado)
          this.estados.splice(index, 1);
          this.estados.unshift(this.cidade.estado);
    })
  }

   getEstados():string[] { 
    return new Array("AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA",
    "PB", "PR", "PE", "PI", "RJ", "RN", "RJ", "RO", "RR", "SC", "SP", "SE", "PO");
 } 

  ////METODO RESPONSAVEL PELO CADASTRO DA CIDADE///////////

  OnSubmit(){

    this.cidade.nome = this.formulario.get('nome').value;
    this.cidade.estado = this.formulario.get('estado').value;
    
   this.cidadeService.updateCidade(this.cidade)
   .subscribe(data=>{alert("Cidade Atualizado Com Sucesso...!!!");
   this.router.navigate(["listarcidade"]);
   this.formulario.reset(this.cidade);
   },
   erro => {
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })  
  }

}
