import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Cidade } from 'src/app/Model/Cidade';
import { CidadeService } from 'src/app/Service/cidade.service';

@Component({
  selector: 'app-adicionar-cidade',
  templateUrl: './adicionar-cidade.component.html',
  styleUrls: ['./adicionar-cidade.component.css']
})

      ////////CLASSE COMPONENTE USADO PARA ADICIONAR UMA CIDADE/////////

export class AdicionarCidadeComponent implements OnInit {

  estados:String[]=["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA",
                  "PB", "PR", "PE", "PI", "RJ", "RN", "RJ", "RO", "RR", "SC", "SP", "SE", "PO"];
  estado:String ="";
  cidade:Cidade=new Cidade();

  constructor(private cidadeService:CidadeService, private router:Router, private formBuilder:FormBuilder) { }

           formulario: any = FormGroup;

  ngOnInit(): void {
    this.formulario = this.formBuilder.group({
      nome: [''],
      estado: ['']
    });
  }

  ////METODO RESPONSAVEL PELO CADASTRO DA CIDADE///////////

  OnSubmit(){
    this.cidade.nome = this.formulario.get('nome').value;
    this.cidade.estado = this.formulario.get('estado').value;

    console.log(this.cidade);
    
   this.cidadeService.createCidade(this.cidade)
   .subscribe(data=>{alert("Distrito Cadastrado Com Sucesso...!!!");

   this.router.navigate(["listarcidade"]);
    this.formulario.reset(this.cidade);
   },
   erro => {
    if(erro.status == 400) {
      console.log(erro);
      alert(erro.error.titulo);
    }
  })
  }

}
