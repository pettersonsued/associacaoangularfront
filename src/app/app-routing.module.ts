import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdicionarAlergiaComponent } from './Alergia/adicionar-alergia/adicionar-alergia.component';
import { AtualizarAlergiaComponent } from './Alergia/atualizar-alergia/atualizar-alergia.component';
import { ListarAlergiaComponent } from './Alergia/listar-alergia/listar-alergia.component';
import { AdicionarAlunoComponent } from './Aluno/adicionar-aluno/adicionar-aluno.component';
import { AtualizarAlunoComponent } from './Aluno/atualizar-aluno/atualizar-aluno.component';
import { ListarAlunoComponent } from './Aluno/listar-aluno/listar-aluno.component';
import { AdicionarCartorioComponent } from './Cartorio/adicionar-cartorio/adicionar-cartorio.component';
import { AtualizarCartorioComponent } from './Cartorio/atualizar-cartorio/atualizar-cartorio.component';
import { ListarCartorioComponent } from './Cartorio/listar-cartorio/listar-cartorio.component';
import { AdicionarCidadeComponent } from './Cidade/adicionar-cidade/adicionar-cidade.component';
import { AtualizarCidadeComponent } from './Cidade/atualizar-cidade/atualizar-cidade.component';
import { ListarCidadeComponent } from './Cidade/listar-cidade/listar-cidade.component';
import { AdicionarDistritoComponent } from './Distrito/adicionar-distrito/adicionar-distrito.component';
import { AtualizarDistritoComponent } from './Distrito/atualizar-distrito/atualizar-distrito.component';
import { ListarDistritoComponent } from './Distrito/listar-distrito/listar-distrito.component';
import { AdicionarEscolaComponent } from './Escola/adicionar-escola/adicionar-escola.component';
import { AtualizarEscolaComponent } from './Escola/atualizar-escola/atualizar-escola.component';
import { ListarEscolaComponent } from './Escola/listar-escola/listar-escola.component';
import { AdicionarIrmaoComponent } from './Irmao/adicionar-irmao/adicionar-irmao.component';
import { AtualizarIrmaoComponent } from './Irmao/atualizar-irmao/atualizar-irmao.component';
import { ListarIrmaoComponent } from './Irmao/listar-irmao/listar-irmao.component';
import { AdicionarMaeComponent } from './Mae/adicionar-mae/adicionar-mae.component';
import { AtualizarMaeComponent } from './Mae/atualizar-mae/atualizar-mae.component';
import { ListarMaeComponent } from './Mae/listar-mae/listar-mae.component';
import { AdicionarPaiComponent } from './Pai/adicionar-pai/adicionar-pai.component';
import { AtualizarPaiComponent } from './Pai/atualizar-pai/atualizar-pai.component';
import { ListarPaiComponent } from './Pai/listar-pai/listar-pai.component';
import { AdicionarProblesaudeComponent } from './Problesaude/adicionar-problesaude/adicionar-problesaude.component';
import { AtualizarProblesaudeComponent } from './Problesaude/atualizar-problesaude/atualizar-problesaude.component';
import { ListarProblesaudeComponent } from './Problesaude/listar-problesaude/listar-problesaude.component';
import { AdicionarRuaComponent } from './Rua/adicionar-rua/adicionar-rua.component';
import { AtualizarRuaComponent } from './Rua/atualizar-rua/atualizar-rua.component';
import { ListarRuaComponent } from './Rua/listar-rua/listar-rua.component';


const routes: Routes = [
  {path:'listarrua', component: ListarRuaComponent},
  {path:'adicionarrua', component: AdicionarRuaComponent},
  {path:'atualizarrua', component: AtualizarRuaComponent},
  {path:'listardistrito', component: ListarDistritoComponent},
  {path:'adicionardistrito', component: AdicionarDistritoComponent},
  {path:'atualizardistrito', component: AtualizarDistritoComponent},
  {path:'listarcidade', component: ListarCidadeComponent},
  {path:'adicionarcidade', component: AdicionarCidadeComponent},
  {path:'atualizarcidade', component: AtualizarCidadeComponent},
  {path:'listarescola', component: ListarEscolaComponent},
  {path:'adicionarescola', component: AdicionarEscolaComponent},
  {path:'atualizarescola', component: AtualizarEscolaComponent},
  {path:'listarcartorio', component: ListarCartorioComponent},
  {path:'adicionarcartorio', component: AdicionarCartorioComponent},
  {path:'atualizarcartorio', component: AtualizarCartorioComponent},
  {path:'listarpai', component: ListarPaiComponent},
  {path:'adicionarpai', component: AdicionarPaiComponent},
  {path:'atualizarpai', component: AtualizarPaiComponent},
  {path:'listarmae', component: ListarMaeComponent},
  {path:'adicionarmae', component: AdicionarMaeComponent},
  {path:'atualizarmae', component: AtualizarMaeComponent},
  {path:'listarproblesaude', component: ListarProblesaudeComponent},
  {path:'adicionarproblesaude', component: AdicionarProblesaudeComponent},
  {path:'atualizarproblesaude', component: AtualizarProblesaudeComponent},
  {path:'listaralergia', component: ListarAlergiaComponent},
  {path:'adicionaralergia', component: AdicionarAlergiaComponent},
  {path:'atualizaralergia', component: AtualizarAlergiaComponent},
  {path:'listarirmao', component: ListarIrmaoComponent},
  {path:'adicionarirmao', component: AdicionarIrmaoComponent},
  {path:'atualizarirmao', component: AtualizarIrmaoComponent},
  {path:'listaraluno', component: ListarAlunoComponent},
  {path:'adicionaraluno', component: AdicionarAlunoComponent},
  {path:'atualizaraluno', component: AtualizarAlunoComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
